$(function(){

	$("body").on('change', '.file-cropper', function(){
		$allowedExtension = ['jpg', 'jpeg', 'gif', 'png'];

		var filename = $(this).val();
		var extension = filename.replace(/^.*\./, '');
		extension = extension.toLowerCase();
		if($.inArray(extension, $allowedExtension) < 0){
			//wrong file
			e.preventDefault();
			sweet_alert('error', 'Please upload valid image type only (JPG, PNG, GIF)');
		}
		else{
			//manage ratio
			$ratioX = $(this).attr('data-x');
			$ratioY = $(this).attr('data-y');

			initializeCropper($(this), [$ratioX, $ratioY]);
			$(this).val('');
		}
	});



	$('body').on('click', '.upload-result', function (ev) {
		canvas = getCroppedCanvas();
		result = canvas.toDataURL('image/jpeg');
		run_cropper(
			result, 
			BASE_URL + '/api/cropper', 
			$(this).attr('data-image-holder'), 
			$(this).attr('data-after-upload')
		);
	});



	$("body").on('click', '.remove-upload', function(){
		filename = $(this).attr('data-hash');
		instance = $(this).closest('.image-holder');

		$.ajax({
			url : BASE_URL + '/api/cropper/remove',
			type : 'POST',
			dataType : 'json',
			data : {
				filename : filename
			},
			success : function(resp){
				sweet_alert(resp.type, resp.message);
				if(resp.type == 'success'){
					instance.fadeOut(250);
					setTimeout(function(){
						instance.remove();
					}, 300);

					$("input[data-hash='"+filename+"']").val('');
				}
			},
			error : function(resp){
				sweet_alert('error', 'Sorry, we cannot process your request right now');
			}
		});
	});

});



//manage cropper js
function initializeCropper(instance, ratio=[]){
	saveTarget = instance.attr('data-fileholder');
	imgTarget = instance.attr('data-imageholder');

	console.log(saveTarget, imgTarget);

	//always destroy before initialize();
	destroyCropper();

	$("#cropperModal").modal('show');
	var f = instance[0].files[0];
	fileurl = window.URL.createObjectURL(f);
	$("img#cropper-image").attr('src', fileurl);

	$(".upload-result").attr('data-after-upload', saveTarget);
	$(".upload-result").attr('data-image-holder', imgTarget);

	if(ratio[0] == 0 || ratio[1] == 0){
		ratioFinal = NaN;
	}
	else{
		rx = ratio[0];
		ry = ratio[1];
		ratioFinal = rx/ry;
	}

	//get best canvas ratio
	$windowWidth = $(window).width();
	$windowHeight = $(window).height();
	$cw = $windowWidth * 0.7;
	$ch = $cw * 2/3;

	if($ch > ($windowHeight * 0.7)){
		$ch = $windowHeight * 0.7;
	}


	$("#cropper-image").cropper({
		aspectRatio : ratioFinal,
		minContainerWidth : $cw,
		minContainerHeight : $ch
	});
}



function blankCropper(){
	$(".cropper-holder").html('');
	$("[data-cropper-container] input[type=hidden]").val('');
}

function destroyCropper(){
	$("#cropper-image").cropper('destroy');
}

function getCroppedCanvas(){
	var canvas = $("#cropper-image").cropper('getCroppedCanvas', {
		width : 800,
		height : 500
	});	
	return canvas;
}

function run_cropper(image, targetUrl, custom_target='', filedata=''){
	if(custom_target.length == 0){
		custom_target = ".after-upload-box";
	}
	console.log(custom_target, filedata);
	$.ajax({
		url: targetUrl,
		type: "POST",
		data: {"image":image},
		dataType : 'json',
		success : function(resp){
			$("#cropperModal").modal('hide');
			$(custom_target).html(resp.html);

			if($(filedata) !== undefined){
				$(filedata).val(resp.name);
				$(filedata).attr('data-hash', resp.name);
			}

		}
	});
}