<?php

namespace Admin\Providers;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $dir = __DIR__."/../../admin";

    public function boot()
    {
        //
        $this->loadRoutesFrom(realpath($this->dir)."/Http/Routes/web.php");
        $this->loadMigrationsFrom(realpath($this->dir)."/Migrations");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        include realpath($this->dir)."/Http/Routes/web.php";
        $this->loadViewsFrom(realpath($this->dir)."/Views/", 'dashboard');
    }
}
