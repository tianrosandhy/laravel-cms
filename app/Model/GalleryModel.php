<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Input;
use Image;
use Storage;

class GalleryModel extends Model
{
    //
    public $resize_to = [];
    public $dir = "/";
    public $file;
    public $filename;


    public function __construct($arr){
    	$this->resize_to = $arr;
    }

    public function upload($file){
    	$this->file = $file;
    	if($this->file->isValid()){
    		//upload ke direktori utama
    		$this->filename = $file->store($this->dir);
    		$this->resize();
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public function resize(){
    	$imgname = $this->filename;
        $path = Storage::path($imgname);
    	foreach($this->resize_to as $batas){

    		$image = Image::make($path);
    		$image->resize($batas, null, function($const){
    			$const->aspectRatio();
    		});
    		$image->save(Storage::path("thumb/thumb-".$batas."-".$imgname));
    	}
    }

    static function rollback($filename, $dir="/"){
        Storage::delete($filename);
        $thumb = [100, 300, 500];
        foreach($thumb as $th){
            Storage::delete('thumb/thumb-'.$th.'-'.$filename);
        }
    }

}
