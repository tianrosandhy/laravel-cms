<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;


class CrudModel extends Model
{
    //
    public $table;
    public $fillable;
    public $searchable;
    public $required;
    public $data;

    public function __construct($table = 'cms_post', $fillable = []){
    	$this->table = $table;
    	$this->fillable = $fillable;
    }


}
