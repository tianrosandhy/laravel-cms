<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Routing\Router;
use Auth;

use PasswordChanger;

class CheckOldPassword
{

	public $except = [
		'update-password',
		'logout'
	];

	public function handle($request, Closure $next){
		//kondisi middleware : 
		//- user yang sudah login
		//- user yg belum ganti password
		$url = $request->url();

		//kalau sistem ga pake halaman admin, skip
		if(strlen(env('ADMIN_PREFIX')) == 0){
			return $next($request);
		}

		//kalau current url bukan di halaman admin skip
		if(strpos($url, env('ADMIN_PREFIX')) === false){
			return $next($request);
		}

		if(Auth::check()){
			$cek = PasswordChanger::passwordUpdated();
			if(!$cek){
				return $next($request);
			}

			foreach($this->except as $except){
				if(strpos($url, $except) !== false){
					return $next($request);
				}
			}

			echo $cek;
			exit();

		}

		return $next($request);
	}


}