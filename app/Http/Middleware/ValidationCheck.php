<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Helper\Cms;

class ValidationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $except = array("/");

    public function handle($request, Closure $next)
    {
        //hanya dilakukan selain halaman homepage
        $cek = Auth::user();
        if(!$cek){
            return redirect()->route('admin.login')->with("error", "Please log in first");
        }

        return $next($request);
    }
}
