<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ForceHttps
{
    public function handle($request, Closure $next, $guard = null)
    {
        $isSecure = $request->header('x-forwarded-proto') == 'https';
        $isForceHttps = env('APP_FORCE_HTTPS') == true;

        if( $isForceHttps )
        {
            $host = $_SERVER['HTTP_HOST'];
            if (!preg_match('/^www\..*/', $host)) {
                return redirect(env('APP_HTTPS_URL'));
            }
        }

        if (!$isSecure && $isForceHttps) {
            return redirect(env('APP_HTTPS_URL'));
        }

        if ($isForceHttps) {
            URL::forceSchema('https');
        }

        return $next($request);
    }
}
