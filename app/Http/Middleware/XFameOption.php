<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\URL;

class XFameOption
{
    protected $router;
    protected $exception_action;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->exception_action = [
            'redirect.social.auth',
        ];
    }

    public function handle($request, Closure $next)
    {
        $action_name = ($this->router->getRoutes()->match($request)->getName()!=null) ? $this->router->getRoutes()->match($request)->getName() : $request->url();
        
        $response = $next($request);
        if(!$this->strposCheck($action_name, $this->exception_action)) {
        	$response->header('X-Frame-Options', 'DENY');
        }

        return $response;
    }

    private function strposCheck($haystack, $needles=[]) {
        $chr = array();
        foreach($needles as $key=>$needle) {
            $res = preg_match('/'.$needle.'/', $haystack);
            if ($res != 0) {
                $chr[$key] = $res;
            }
        }

        if(empty($chr)) return false;
        return min($chr);
    }
}
