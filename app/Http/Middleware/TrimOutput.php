<?php
namespace App\Http\Middleware;

use Closure;

class TrimOutput
{
	public function handle($request, Closure $next){
		//disable trimming for ajax request
		if($request->ajax()){
			return $next($request);
		}
		$response = $next($request);
		$output = $response->getOriginalContent();

		$filters = array(
			'/(?<!\S)\/\/\s*[^\r\n]*/'	=> '', // Remove comments in the form /* */
			'/\s{2,}/'			=> ' ', // Shorten multiple white spaces
			'/(\r?\n)/'			=> '', // Collapse new lines
		);
		
		$output = preg_replace(array_keys($filters), array_values($filters), $output);
		$response->setContent($output);

		return $response;
	}
}
