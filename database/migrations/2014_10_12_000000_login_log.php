<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_login_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->text('server_info');
            $table->string('username');
            $table->timestamps();
            $table->string('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_login_log');
    }
}
