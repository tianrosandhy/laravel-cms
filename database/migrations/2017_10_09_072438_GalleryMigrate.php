<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GalleryMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_gallery', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('label');
            $tb->string('filename',255);
            $tb->string('directory');
            $tb->string('description');
            $tb->string('attributes', 500);
            $tb->tinyinteger('stat');
            $tb->string('username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_gallery');
    }
}
