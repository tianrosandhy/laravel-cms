<?php

use Illuminate\Database\Seeder;

class DefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $username = "admin";
        $email = 'admin@localhost';
        $default_pass = "admin";
        $priviledge = 1;

        DB::table('users')->insert([
        	'username' => $username,
        	'email' => $email,
        	'password' => password_hash($default_pass, PASSWORD_DEFAULT),
        	'remember_token' => sha1(rand(1,1000000)),
        	'priviledge' => 1
        ]);
    }
}
