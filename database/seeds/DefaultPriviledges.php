<?php

use Illuminate\Database\Seeder;

class DefaultPriviledges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users_priviledge')->insert([
        	'priviledge_name' => 'Administrator',
            'permission' => '["post.index","post.store","post.edit","post.update","post.destroy","postcategory.index","postcategory.store","postcategory.edit","postcategory.update","postcategory.destroy","page.index","page.store","page.edit","page.update","page.destroy","navigation.index","navigation.store","navigation.edit","navigation.update","navigation.destroy","nav.index","nav.store","nav.edit","nav.update","nav.destroy","quiz.index","quiz.store","quiz.edit","quiz.update","quiz.destroy","banner.index","banner.store","banner.edit","banner.update","banner.destroy","setting.setting.index","setting.setting.store","setting.user.index","setting.user.store","setting.user.edit","setting.user.update","setting.user.destroy","setting.priviledge.index","setting.priviledge.store","setting.priviledge.edit","setting.priviledge.update","setting.priviledge.manage","setting.priviledge.destroy"]',
            'type' => 0,
            'stat' => 1
        ]);
    }
}
