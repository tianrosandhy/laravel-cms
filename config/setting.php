<?php
return [
	'General Setting' => [

		'name' => [
			'label' => 'Website Name',
			'description' => 'Nama website yang akan muncul di front end',
			'type' => 'text', //input type
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		'subtitle' => [
			'label' => 'Website Sub Title',
			'description' => 'Sub judul yang akan muncul dibawah title',
			'list' => [],
			'type' => 'text', //input type
			'default' => 'Powered by Laravel CMS',
			'attr' => ['class' => 'form-control'],
		],

		'copyright' => [
			'label' => 'Copyright Information',
			'description' => '',
			'type' => 'text',
			'default' => 'Copyright &copy; 2017',
			'attr' => ['class' => 'form-control']
		],

		'background' => [
			'label' => 'Backend Login Background',
			'description' => '',
			'type' => 'cropper',
			'default' => null,
			'ratio' => [1000, 618],
			'attr' => []
		],

		'favicon' =>[
			'label' => 'PNG Favicon',
			'description' => 'File icon format PNG untuk website icon',
			'type' => 'file',
			'default' => null,
			'attr' => []
		],

		'logo' => [
			'label' => 'Website Logo',
			'description' => 'Logo website yang akan ditampilkan',
			'type' => 'file',
			'default' => NULL,
			'attr' => [],
		],

		'login_attempt' => [
			'label' => 'Maximum Login Attempt',
			'description' => 'Jumlah kesalahan login maksimum',
			'type' => 'number',
			'default' => 10,
			'attr' => ['class' => 'form-control']
		],


	],


	'Contact Information' => [
		'address' => [
			'label' => "Address",
			'description' => '',
			'type' => 'textarea',
			'default' => 'Company address',
			'attr' => ['class' => 'form-control'],
		],
		'phone' => [
			'label' => "Phone Number",
			'description' => '',
			'type' => 'tel',
			'default' => '+6200 0000 0000',
			'attr' => ['class' => 'form-control'],
		],
		'email' => [
			'label' => "Company Email",
			'description' => '',
			'type' => 'email',
			'default' => 'contact@company.mail.com',
			'attr' => ['class' => 'form-control'],
		],
	],


	'Social Setting' => [
		'facebook' => [
			'label' => 'Facebook URL',
			'description' => 'Alamat URL Facebook Client',
			'type' => 'text',
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		
		'twitter' => [
			'label' => 'Twitter URL',
			'description' => 'Alamat URL Twitter Client',
			'type' => 'text',
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		
		'instagram' => [
			'label' => 'Instagram URL',
			'description' => 'Alamat URL Instagram Client',
			'type' => 'text',
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		
		'youtube' => [
			'label' => 'Youtube URL',
			'description' => 'Alamat URL Youtube Client',
			'type' => 'text',
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		
	],



	'API Keys' => [
		'fb_app' => [
			'label' => 'Facebook App ID',
			'description' => '',
			'type' => 'text',
			'default' => '1984168668502533',
			'attr' => ['class' => 'form-control']
		],
		'fb_app_secret' => [
			'label' => 'Facebook App Secret',
			'description' => '',
			'type' => 'text',
			'default' => '8c6feb7286cde80b53f367ad185639a0',
			'attr' => ['class' => 'form-control']
		],
		'twitter_consumer_key' => [
			'label' => 'Twitter Consumer Key',
			'description' => '',
			'type' => 'text',
			'default' => '7qjzDP6dKzDfZL2tbxP9LxMfj',
			'attr' => ['class' => 'form-control']
		],
		'twitter_consumer_secret' => [
			'label' => 'Twitter Consumer Secret',
			'description' => '',
			'type' => 'text',
			'default' => 'VQjEvHPrJK3b039gYi0w7BvdK9BJVcQOwYGrJ5zh6kAkus0v6d',
			'attr' => ['class' => 'form-control']
		],

	],




	'Search Engine Optimazion' => [
		'seo_description' => [
			'label' => 'SEO Description',
			'description' => 'deskripsi website untuk SEO',
			'type' => 'textarea',
			'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, corporis, debitis dolores fugiat error sequi.',
			'attr' => ['class' => 'form-control'],
		],

		'seo_keywords' => [
			'label' => 'SEO Keyword',
			'description' => 'kata kunci website untuk SEO',
			'type' => 'text',
			'default' => '',
			'attr' => ['class' => 'form-control'],
		],

		'seo_image' => [
			'label' => 'SEO Default Image',
			'description' => 'File gambar default website untuk SEO',
			'type' => 'cropper',
			'default' => null,
			'attr' => []
		],
		
	],

];