<?php
return [
	0 => [
		'label' => 'Price',
		'name' => 'price',
		'type' => 'text',
		'attr' => ['class' => 'form-control price-holder autonumeric', 'data-a-sign' => "Rp "]
	],

	1 => [
		'label' => 'Color',
		'name' => 'color',
		'type' => 'color',
		'attr' => ['class' => 'form-control']
	],

	2 => [
		'label' => 'Size',
		'name' => 'size',
		'type' => 'select',
		'list' => [
			'S' => 'S',
			'M' => 'M',
			'L' => 'L',
			'XL' => 'XL',
			'XXL' => 'XXL'
		],
		'attr' => ['class' => 'form-control']
	],

];