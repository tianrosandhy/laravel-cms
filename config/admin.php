<?php
return [
	'menu' => [
		'Dashboard' => [
			'target' => 'admin.dashboard',
			'icon' => 'fa-home'
		],
		'Post' => [
			'target' => null,
			'icon' => 'fa-book',
			'submenu' => [
				'All Post' => 'admin.post.index',
				'Post Category' => 'admin.postcategory.index'
			]
		],

		'Product' => [
			'target' => 'admin.product.index',
			'icon' => 'fa-shopping-cart',
		],

		'Page' => [
			'target' => 'admin.page.index',
			'icon' => 'fa-file-o',
		],
		'Navigation' => [
			'target' => null,
			'icon' => 'fa-list',
			'submenu' => [
				'Navigation Group' => 'admin.navigation.index',
				'Navigation Item' => 'admin.nav.index'
			]
		],

		'Banner' => [
			'target' => 'admin.banner.index',
			'icon' => 'fa-image'
		],

		'Quiz' => [
			'target' => 'admin.quiz.index',
			'icon' => 'fa-question-circle'
		],

		'Setting' => [
			'target' => null,
			'icon' => 'fa-cog',
			'submenu' => [
				'General' => 'admin.setting.setting.index',
				'User' => 'admin.setting.user.index',
				'Multi Language' => 'admin.lang.index',
				'Priviledge' => 'admin.setting.priviledge.index',
			]
		],
		"Logout" => [
			'target' => 'admin.logout',
			'icon' => 'fa-sign-out'
		]


	],

];