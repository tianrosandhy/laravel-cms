<?php
header("HTTP/1.0 404 Not Found");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <base href="{{ url('/') }}/">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <title>Page Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{!! assets('pages/ico/60.png') !!}">
    <link rel="apple-touch-icon" sizes="76x76" href="{!! assets('pages/ico/76.png') !!}">
    <link rel="apple-touch-icon" sizes="120x120" href="{!! assets('pages/ico/120.png') !!}">
    <link rel="apple-touch-icon" sizes="152x152" href="{!! assets('pages/ico/152.png') !!}">
    <link rel="icon" type="image/x-icon" href="{{ assets('favicon.ico') }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    {!! register_single('assets/plugins/pace/pace-theme-flash.css', 'css') !!}
    {!! register_single('assets/plugins/bootstrap/css/bootstrap.min.css', 'css') !!}
    {!! register_single('assets/plugins/jquery-scrollbar/jquery.scrollbar.css', 'css') !!}
    {!! register_single('assets/plugins/alerts/css/alertify.min.css', 'css') !!}
    {!! register_single('pages/css/themes/light.css', 'css') !!}

    {!! register_single('assets/plugins/font-awesome/css/font-awesome.css', 'css') !!}
    {!! register_single('pages/css/pages-icons.css', 'css') !!}

    {!! register_single('custom.css', 'css') !!}

    {!! register_single('assets/plugins/jquery/jquery-1.11.1.min.js', 'js') !!}
  
    @yield('custom-style')
  </head>
  <body class="fixed-header error-page">
    <div class="d-flex justify-content-center full-height full-width align-items-center">
      <div class="error-container text-center">
        <h1 class="error-number">404</h1>
        <h2 class="semi-bold">Sorry but we couldnt find this page</h2>
        <p class="p-b-10">
        	This page you are looking for does not exist
        	<br>
        	<a href="{{ url('/') }}" class="btn btn-primary">Back to Homepage</a>
        </p>
      </div>
    </div>

    {!! Cache::merge([
      'assets/plugins/pace/pace.min.js',
      'assets/plugins/modernizr.custom.js',
      'assets/plugins/jquery-ui/jquery-ui.min.js',
      'assets/plugins/tether/js/tether.min.js',
      'assets/plugins/bootstrap/js/bootstrap.min.js',
      'assets/js/popper.min.js',
      'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
      
      'custom/jquery-debounce.js',
      'assets/js/sweetalert.min.js',
    ], [
      'dir' => 'admin_theme/',
      'type' => 'js'
    ]) !!}
    
    {!! register_single('pages/js/pages.min.js', 'js') !!}
    {!! register_single('assets/js/scripts.js', 'js') !!}
  </body>
</html>