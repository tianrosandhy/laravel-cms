<div id="modalForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{{ Form::open(['method' => 'POST', 'class' => 'ajax-form']) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Tambah Data</h3>
			</div>
			<div class="modal-body">
				@foreach($forms as $label => $input)
				<div class="row">
					<div class="col-sm-3">
						{{ $label }}
					</div>
					<div class="col-sm-9">
						{!! $input !!}

					</div>
				</div>
				@endforeach

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span>
					Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>