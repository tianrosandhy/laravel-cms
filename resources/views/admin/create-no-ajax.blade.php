<div id="modalForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="backend/setting/priviledge/update/2" method="post">
			{{ csrf_field() }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Tambah Data</h3>
			</div>
			<div class="modal-body">
				@foreach($forms as $label => $input)
				<div class="row">
					<div class="col-sm-3">
						{{ $label }}
					</div>
					<div class="col-sm-9">
						{!! $input !!}

					</div>
				</div>
				@endforeach
				<div class="row append">
					<div class="col-sm-3">Current Image Uploaded</div>
					<div class="col-sm-9">
						<img src="">
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary" value="Simpan">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>