@extends ('admin.inc.template')
@section ('content')

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ isset($title) ? $title : "" }}</h1>
		</div>
	</div><!--/.row-->
	
@stop