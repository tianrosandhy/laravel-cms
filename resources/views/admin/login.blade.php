<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <base href="{{ url('/') }}/">
    <title>Login Page - {{ Setting::getParam('name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    {!! Cache::merge([
      'assets/plugins/pace/pace-theme-flash.css',
      'assets/plugins/bootstrap/css/bootstrap.min.css',
      'assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
    ], 'css') !!}

    {!! register_single('pages/css/themes/corporate.css', 'css') !!}
    {!! register_single('assets/plugins/font-awesome/css/font-awesome.css', 'css') !!}  
    {!! register_single('pages/css/pages-icons.css', 'css') !!}  

    @yield('additional_style')
    @stack ('style')
    @stack ('styles')

  </head>
  <body class="fixed-header menu-pin menu-behind">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        @if(strlen(Setting::getParam('background')) == 0)
        <img src="{!! assets('assets/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') !!}" alt="" class="lazy">
        @else
        <img src="{!! url('storage/'.Setting::getParam('background')) !!}" alt="" class="lazy">
        @endif

        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          <h2 class="semi-bold text-white">
					 {{ Setting::getParam('name') }}
          </h2>
          <p class="small">
            {{ Setting::getParam('subtitle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, eos, esse, non error facilis ad maiores eum quae vero libero voluptas voluptate? Reprehenderit, sunt similique quae.') }}
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          @include ('admin.inc.partials.logo')

          <!-- START Login Form -->
          <form id="form-login" class="p-t-15" role="form" action="" method="post">
            {{ csrf_field() }}
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Email</label>
              <div class="controls">
                <input type="text" name="email" placeholder="Email" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Password</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Credentials" required>
              </div>
            </div>
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-6 no-padding sm-p-l-10">
                <div class="checkbox ">
                  <input type="checkbox" name="remember" value="1" id="checkbox1">
                  <label for="checkbox1">Keep Me Signed in</label>
                </div>
              </div>
              <div class="col-md-6 d-flex align-items-center justify-content-end">
                <span class="text-info small">Still not have an account? <a href="{{ admin_url('register') }}">Create Account!</a></span>
              </div>
            </div>
            <!-- END Form Control-->
            <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
          </form>
         
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    
    <!-- BEGIN VENDOR JS -->
    {!! Cache::merge([
      'assets/plugins/pace/pace.min.js',
      'assets/plugins/jquery/jquery-1.11.1.min.js',
      'assets/plugins/modernizr.custom.js',
      'assets/plugins/tether/js/tether.min.js',
      'assets/plugins/bootstrap/js/bootstrap.min.js',
      'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
      
      'custom/jquery-debounce.js',
      'assets/js/sweetalert.min.js',
      'assets/plugins/alerts/alertify.min.js',
      'assets/plugins/summernote/summernote-bs4.min.js',
      'pages/js/pages.min.js',
    ], 'js') !!}

    @include('admin.inc.script')
  </body>
</html>