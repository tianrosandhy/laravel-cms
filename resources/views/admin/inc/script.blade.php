{!! register_single('assets/js/be-script.js', 'js') !!}
<script>
//Admin autoload script
var ADMIN_URL = "{{ env('ADMIN_PREFIX') }}";
var BASE_URL = "{{ url('/') }}";
var UPLOADDIR = "{{ config('filesystems.disks.public.url') }}";
var CSRF_TOKEN;

(function($){
	$(function(){
		CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
		
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': CSRF_TOKEN
		    }
		});

	    //sidebar link background
		if($("ul li ul li.active").length > 0){
			$("ul li ul li.active").closest('li.parent').addClass('active');
			$("ul li ul li.active").closest('ul.children').collapse("show");
		}

		@if(session('error'))
			@if(is_array(session('error')))
				@foreach(session('error') as $err => $msg)
					sweet_alert('error', '{{ $msg }}', 'error');
				@endforeach
			@else
				sweet_alert('error', '{{ session('error') }}', 'error');
			@endif

		@elseif(session('success'))
			sweet_alert('success', '{{ session('success') }}', 'success');
		@elseif(session('alert'))
			sweet_alert('alert', '{{ session('alert') }}', 'alert');
		@endif

		@if($errors->any())
			@foreach($errors->all() as $err)
				sweet_alert('error', '{{ $err }}', 'error');
			@endforeach
		@endif

		@yield('custom-script')

	});
})(jQuery);
</script>