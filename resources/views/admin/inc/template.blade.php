<!DOCTYPE html>
<html>
  <head>
    @include ('admin.inc.partials.metadata')
  </head>
  <body class="fixed-header menu-pin">
    <nav class="page-sidebar" data-pages="sidebar">
      <div class="sidebar-header">
        @include ('admin.inc.partials.logo')
      </div>
      @include ('admin.inc.partials.sidebar')
    </nav>


    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      @include ('admin.inc.partials.header')

      <div class="page-content-wrapper ">
        <div class="content ">
          @include ('admin.inc.partials.breadcrumb')

          <div class=" container-fluid   container-fixed-lg">
            @yield('content')
          </div>
        </div>

        <div class=" container-fluid  container-fixed-lg footer">
          @include ('admin.inc.partials.footer')
        </div>
      </div>

    </div>
    <!-- END PAGE CONTAINER -->
    
    @include ('admin.inc.partials.webscript')
  </body>
</html>