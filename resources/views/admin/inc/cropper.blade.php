@push ('style')
    {!! register_single('assets/plugins/cropper/cropper.min.css', 'css') !!}
@endpush
<div id="cropperModal" class="modal" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" data-target="#cropperModal"><i class="fa fa-times"></i></button>
				</div>
				<div class="modal-body">
					<div id="cropper-container">
						<img src="" alt="" id="cropper-image">
					</div>

					<div class="padd" align="center">
						<button class="btn btn-primary upload-result">Crop Image</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@push ('script')
{!! register_single('assets/plugins/cropper/cropper.min.js', 'js') !!}
{!! register_single('assets/js/module-cropper.js', 'js') !!}
@endpush