<?php
$name = isset($name) ? $name : 'uploaded_img';
$value = '';
if(isset($oldimage)){
	//create value instance
	$value = implode('&&', $oldimage);
}
?>
<input type="hidden" name="{{ $name }}" value="{{ $value }}" class="multi-dropzone_uploaded">
<div class="dropzone multi-dropzone" data-target="{{ env('ADMIN_PREFIX') .'/api/gallery' }}"></div>
<div class="multi-image-holder">
	@if(isset($oldimage))
		@foreach($oldimage as $filename)
		<div class="img-item" data-file="{{ $filename }}">
			<img src="{{ url('storage/'. $filename) }}">
			<label>{{ $filename }}</label>
			<span class="remove">&times;</span>
		</div>
		@endforeach
	@endif
</div>

<script>
$(function(){
	Dropzone.autoDiscover = false;
	var ajaxurl = $(".multi-dropzone").data("target");
	$(".multi-dropzone").dropzone({
		url : ajaxurl,
		sending : function(file, xhr, formData){
			formData.append("_token", CSRF_TOKEN);
		},
		init : function(){
			this.on("success", function(file, data){
				oldval = $(".multi-dropzone_uploaded").val();
				if(oldval.length > 0){
					$(".multi-dropzone_uploaded").val( oldval + '&&' + data);
				}
				else{
					$(".multi-dropzone_uploaded").val(data);
				}

				createFileInstance(data);

				this.removeFile(this.files[0]); //clear dropzone on success
			});
			this.on("addedfile", function() {
		      if (this.files[1]!=null){
		        this.removeFile(this.files[0]);
		      }
		    });
		}
	});

});
</script>

@push ('script')
<script>
$(function(){
	$("body").on('click', '.multi-image-holder .img-item .remove', function(){
		instance = $(this).closest('.img-item');
		file = instance.attr('data-file');
		old = $(".multi-dropzone_uploaded").val();
		$(".multi-dropzone_uploaded").val(old.replace(file, '')); //remove input instance
		instance.fadeOut();
		setTimeout(function(){
			instance.remove();
		}, 500);
	});

});

function createFileInstance(filename){
	$(".multi-image-holder").append(
		'<div class="img-item" data-file="'+filename+'"><img src="'+ BASE_URL +'/storage/'+filename+'"><label>'+filename+'</label><span class="remove">&times;</span></div>'
	);
}
</script>
@endpush
