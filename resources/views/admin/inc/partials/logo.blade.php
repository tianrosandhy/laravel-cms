@if(strlen(Setting::getParam('logo')) == 0)
<img src="{!! assets('assets/img/logo.png') !!}" alt="logo" height="22">
@else
  @if(is_file(config('filesystems.disks.public.root').'/'.Setting::getParam('logo')))
    <img src="{!! url('storage/'.Setting::getParam('logo')) !!}" alt="logo" height="22">
  @else
    <img src="{!! assets('assets/img/logo.png') !!}" alt="logo" height="22">          
  @endif
@endif