<?php
$name = isset($name) ? $name : 'uploaded_img';
$value = isset($value) ? $value : '';
$hash = isset($hash) ? $hash : md5(uniqid());

$ratio = isset($ratio) ? $ratio : [0, 0];
?>
<div class="card" data-cropper-container>
	<div class="card-block">
		<input type="hidden" name="{{ $name }}" class="filename-{{ $hash }}" value="{!! $value !!}">
		<label class="cropper-trigger">
			<input type="file" class="file-cropper" data-fileholder=".filename-{{ $hash }}" data-imageholder=".after-upload-{{ $hash }}" data-x="{{ $ratio[0] }}" data-y="{{ $ratio[1] }}" >
			Upload File
		</label>
		<div class="cropper-holder after-upload-{{ $hash }}">
			@if (is_file('storage/'.$value))
			<div class="image-holder">
				<img src="{{ thumb_url($value, 100) }}" style="width:100px" alt="{{ $value }}">
				<br>
				<span class="close-button remove-upload" data-hash="{{ $value }}">x</span>
			</div>
			@endif
		</div>
		
	</div>
</div>