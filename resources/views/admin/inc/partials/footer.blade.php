<div class="copyright sm-text-center">
	<p class="small no-margin pull-left sm-pull-reset">
	  <span class="hint-text">Copyright &copy; 2017 </span>
	  <span class="font-montserrat">Maxsol</span>.
	  <span class="hint-text">All rights reserved. </span>
	</p>
	<p class="small no-margin pull-right sm-pull-reset">
	  Hand-crafted <span class="hint-text">&amp; made with Love</span>
	</p>
	<div class="clearfix"></div>
</div>