<!-- START HEADER -->
<div class="header ">
	<!-- START MOBILE SIDEBAR TOGGLE -->
	<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
	</a>
	<!-- END MOBILE SIDEBAR TOGGLE -->
	<div class="">
	  <div class="brand inline">
	    @include ('admin.inc.partials.logo')
	  </div>
	</div>

	<div class="d-flex align-items-center">
	  <!-- START User Info-->
	  <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
	    <span class="semi-bold">{{ Auth::user()['username'] }}</span>
	  </div>
	  <div class="dropdown pull-right hidden-md-down">
	    <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	      <span class="thumbnail-wrapper d32 circular inline">
	      <img src="{{ assets('assets/img/profiles/avatar.jpg') }}" alt="" width="32" height="32">
	      </span>
	    </button>
	    <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
	   	  <a href="{{ url('/') }}" target="_blank" class="dropdown-item"><i class="fa fa-home"></i> Go to Main Site</a>
	      <a href="{{ url(env('ADMIN_PREFIX').'/'.'setting') }}" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
	      <a href="{{ url(env('ADMIN_PREFIX').'/'.'logout') }}" class="clearfix bg-master-lighter dropdown-item">
	        <span class="pull-left">Logout</span>
	        <span class="pull-right"><i class="pg-power"></i></span>
	      </a>
	    </div>
	  </div>
	  <!-- END User Info-->
	</div>
</div>
<!-- END HEADER -->