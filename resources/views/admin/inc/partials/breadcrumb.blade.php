<div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
	<div class="inner">
		<!-- START BREADCRUMB -->

		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="{{ admin_url('') }}">Home</a></li>
			<?php
			if(!isset($breadcrumbs)) :
			$current = Request::route()->getName();
			//DEFAULT BREADCRUMB BY ROUTE
			foreach(config('admin') as $list){
				foreach($list as $label=>$data){
					if($data['target'] == $current){
						echo '<li class="breadcrumb-item active"><a href="'.url()->route($data['target']).'">'.$label.'</a></li>';
					}

					if(isset($data['submenu'])){
						foreach($data['submenu'] as $sublbl => $subroute){
							if($subroute == $current){
								echo '<li class="breadcrumb-item"><a href="#">'.$label.'</a></li>';
								echo '<li class="breadcrumb-item active"><a href="'.url()->route($subroute).'">'.$sublbl.'</a></li>';
							}
						}
					}
				}
			}
			//CUSTOM BREADCRUMB
			else :
				$cnt = count($breadcrumbs);
				$inc = 0;
				foreach($breadcrumbs as $blabel => $burl){
					$inc++;

					echo '<li class="breadcrumb-item '.(($inc == $cnt) ? 'active' : '').'"><a href="'.$burl.'">'.$blabel.'</a></li>';
				}

			endif;
			?>
		</ol>
		<!-- END BREADCRUMB -->
	</div>
</div>