<!-- START SIDEBAR MENU -->
<div class="sidebar-menu">
	<!-- BEGIN SIDEBAR MENU ITEMS-->
	<ul class="menu-items">
	  @foreach(config('admin.menu') as $label => $menu )
	    @if(Setting::adminCheckPermission($menu['target'], (isset($menu['submenu']) ? $menu['submenu'] : []) ))
	    <li class="{{ Request::route()->getName() == $menu['target'] ? 'active' : '' }} {{ $menu['target'] === null ? 'parent' : '' }}">
	      <a href="{{ $menu['target'] === null ? 'javascript:;' : url(route($menu['target'])) }}">
	          <span class="title">{{ $label }}</span>

	      @if(isset($menu['submenu']))
	      <span class=" arrow"></span>
	      @endif
	      </a>
	      <span class="icon-thumbnail"><i class="fa {{ $menu['icon'] }}"></i></span>

	      @if(isset($menu['submenu']))
	      <ul class="sub-menu">
	        @foreach($menu['submenu'] as $sublbl => $target)
	          @if(Setting::adminCheckPermission($target))
	          <li {!! $target == Request::route()->getName() ? 'class="active"' : "" !!}>
	            <a href="{{ url(route($target)) }}">{{ $sublbl }}</a>
	            <span class="icon-thumbnail">c</span>
	          </li>
	          @endif
	        @endforeach
	      </ul>
	      @endif
	    </li>
	    @endif
	  @endforeach

	</ul>
	<div class="clearfix"></div>
</div>
<!-- END SIDEBAR MENU -->