    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <base href="{{ url('/') }}/">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <title>{{ isset($title) ? $title." - " : "" }} {{ Setting::getParam('name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{!! assets('pages/ico/60.png') !!}">
    <link rel="apple-touch-icon" sizes="76x76" href="{!! assets('pages/ico/76.png') !!}">
    <link rel="apple-touch-icon" sizes="120x120" href="{!! assets('pages/ico/120.png') !!}">
    <link rel="apple-touch-icon" sizes="152x152" href="{!! assets('pages/ico/152.png') !!}">
    <link rel="icon" type="image/x-icon" href="{{ assets('favicon.ico') }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    {!! register_single('assets/plugins/pace/pace-theme-flash.css', 'css') !!}
    {!! register_single('assets/plugins/bootstrap/css/bootstrap.min.css', 'css') !!}
    {!! register_single('assets/plugins/jquery-scrollbar/jquery.scrollbar.css', 'css') !!}
    {!! register_single('assets/plugins/alerts/css/alertify.min.css', 'css') !!}
    {!! register_single('assets/plugins/select2/css/select2.min.css', 'css') !!}
    {!! register_single('assets/plugins/switchery/css/switchery.min.css', 'css') !!}
    {!! register_single('assets/plugins/summernote/summernote-bs4.css', 'css') !!}
    {!! register_single('assets/plugins/jquery-nestable/jquery.nestable.min.css', 'css') !!}
    {!! register_single('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css', 'css') !!}
    {!! register_single('pages/css/themes/light.css', 'css') !!}
    {!! register_single('assets/plugins/bootstrap-tokenfield/css/bootstrap-tokenfield.css', 'css') !!}
    {!! register_single('assets/plugins/bootstrap-tokenfield/css/tokenfield-typeahead.min.css', 'css') !!}
    {!! register_single('assets/plugins/dropzone/css/dropzone.css', 'css') !!}
    {!! register_single('assets/plugins/font-awesome/css/font-awesome.css', 'css') !!}
    {!! register_single('pages/css/pages-icons.css', 'css') !!}

    {!! register_single('custom.css', 'css') !!}

    {!! register_single('assets/plugins/jquery/jquery-1.11.1.min.js', 'js') !!}
  
    @yield('custom-style')
    @stack ('style')
    @stack ('styles')