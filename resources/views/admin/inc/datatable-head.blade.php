<thead>
	<tr>
		@foreach($columns as $key=>$val)
			<th>{{ $key }}</th>
		@endforeach
	</tr>
</thead>
<thead class="search">
	<tr>
		@foreach($columns as $key=>$val)
			<th {!! isset($val['custom']) ? 'data-custom field="'.$val['col'].'"' : '' !!}>{{ $val['search'] ? $val['col'] : "" }}</th>
		@endforeach
	</tr>
</thead>