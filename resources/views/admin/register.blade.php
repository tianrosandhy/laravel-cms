
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<title>Register Page - {{ Setting::getParam('name') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
	<link rel="apple-touch-icon" href="pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
	<link rel="icon" type="image/x-icon" href="favicon.ico" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="" name="author" />
    {!! Cache::merge([
      'assets/plugins/pace/pace-theme-flash.css',
      'assets/plugins/bootstrap/css/bootstrap.min.css',
      'assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
    ], 'css') !!}

    {!! register_single('pages/css/themes/corporate.css', 'css') !!}
    {!! register_single('assets/plugins/font-awesome/css/font-awesome.css', 'css') !!}  
    {!! register_single('pages/css/pages-icons.css', 'css') !!}  

    @yield('additional_style')
    @stack ('style')
    @stack ('styles')

</head>
<body class="fixed-header menu-pin">
<div class="register-container full-height sm-p-t-30">
  <div class="d-flex justify-content-center flex-column full-height ">
    
   <div>
     @include ('admin.inc.partials.logo')
   </div>
	 <h3>Register Now</h3>

     <form id="form-register" class="p-t-15" role="form" action="" method="post">
     	{!! csrf_field() !!}
        <div class="row">
           <div class="col-md-12">
              <div class="form-group form-group-default">
                 <label>Username</label>
                 <input type="text" name="username" class="form-control" required value="{{ old('username') }}">
              </div>
           </div>
        </div>
        <div class="row">
           <div class="col-md-12">
              <div class="form-group form-group-default">
                 <label>Email</label>
                 <input type="email" name="email" placeholder="Your email for log in" class="form-control" required value="{{ old('email') }}">
              </div>
           </div>
        </div>
        <div class="row">
           <div class="col-md-6">
              <div class="form-group form-group-default">
                 <label>Password</label>
                 <input type="password" name="password" placeholder="Minimum of 6 character" class="form-control" required>
              </div>
           </div>
           <div class="col-md-6">
              <div class="form-group form-group-default">
                 <label>Repeat Password</label>
                 <input type="password" name="password_confirmation" placeholder="Retype your password" class="form-control" required>
              </div>
           </div>

        </div>
        <button class="btn btn-primary btn-cons m-t-10" type="submit">Create a new account</button>
     </form>
  </div>
</div>
<div class=" full-width">
  <div class="register-container m-b-10 clearfix">
     <div class="m-b-30 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix d-flex-md-up">
        <div class="col-md-2 no-padding d-flex align-items-center">
           <img src="assets/img/demo/pages_icon.png" alt="" class="" data-src="assets/img/demo/pages_icon.png" data-src-retina="assets/img/demo/pages_icon_2x.png" width="60" height="60">
        </div>
        <div class="col-md-9 no-padding d-flex align-items-center">
           <p class="hinted-text small inline sm-p-t-10">No part of this website or any of its contents may be reproduced, copied, modified or adapted, without the prior written consent of the author, unless otherwise indicated for stand-alone materials.</p>
        </div>
     </div>
  </div>
</div>
<div class="overlay hide" data-pages="search">
  <div class="overlay-content has-results m-t-20">
     <div class="container-fluid">
        <img class="overlay-brand" src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
        <a href="#" class="close-icon-light overlay-close text-black fs-16">
        <i class="pg-close"></i>
        </a>
     </div>
     <div class="container-fluid">
        <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">
        <br>
        <div class="inline-block">
           <div class="checkbox right">
              <input id="checkboxn" type="checkbox" value="1" checked="checked">
              <label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
           </div>
        </div>
        <div class="inline-block m-l-10">
           <p class="fs-13">Press enter to search</p>
        </div>
     </div>
     <div class="container-fluid">
        <span>
        <strong>suggestions :</strong>
        </span>
        <span id="overlay-suggestions"></span>
        <br>
        <div class="search-results m-t-40">
           <p class="bold">Pages Search Results</p>
           <div class="row">
              <div class="col-md-6">
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                       <div>
                          <img width="50" height="50" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
                       </div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on pages</h5>
                       <p class="hint-text">via john smith</p>
                    </div>
                 </div>
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                       <div>T</div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> related topics</h5>
                       <p class="hint-text">via pages</p>
                    </div>
                 </div>
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                       <div><i class="fa fa-headphones large-text "></i></div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> music</h5>
                       <p class="hint-text">via pagesmix</p>
                    </div>
                 </div>
              </div>
              <div class="col-md-6">
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
                       <div><i class="fa fa-facebook large-text "></i></div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on facebook</h5>
                       <p class="hint-text">via facebook</p>
                    </div>
                 </div>
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
                       <div><i class="fa fa-twitter large-text "></i></div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5">Tweats on<span class="semi-bold result-name"> ice cream</span></h5>
                       <p class="hint-text">via twitter</p>
                    </div>
                 </div>
                 <div class="">
                    <div class="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
                       <div><i class="fa fa-google-plus large-text "></i></div>
                    </div>
                    <div class="p-l-10 inline p-t-5">
                       <h5 class="m-b-5">Circles on<span class="semi-bold result-name"> ice cream</span></h5>
                       <p class="hint-text">via google plus</p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>
<!-- BEGIN VENDOR JS -->
{!! Cache::merge([
  'assets/plugins/pace/pace.min.js',
  'assets/plugins/jquery/jquery-1.11.1.min.js',
  'assets/plugins/modernizr.custom.js',
  'assets/plugins/tether/js/tether.min.js',
  'assets/plugins/bootstrap/js/bootstrap.min.js',
  'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
  
  'custom/jquery-debounce.js',
  'assets/js/sweetalert.min.js',
  'assets/plugins/alerts/alertify.min.js',
  'assets/plugins/summernote/summernote-bs4.min.js',
  'pages/js/pages.min.js',
], 'js') !!}

@include('admin.inc.script')
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
  $(function()
  {
    $('#form-register').validate()
  });
</script>
</body>
</html>
