<?php 

namespace Module\Quiz\Facade;

use Illuminate\Support\Facades\Facade;

class QuizFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Quiz\\Controllers\\QuizController';
    }
}