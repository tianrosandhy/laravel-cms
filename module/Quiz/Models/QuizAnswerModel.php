<?php

namespace Module\Quiz\Models;

use Illuminate\Database\Eloquent\Model;


class QuizAnswerModel extends Model
{
    //
    protected $table = "maxvel_quiz_answer";
    protected $primaryKey = 'answer_id';
    protected $fillable = [
    	'answer_id',
        'answer',
        'quiz_id',
        'score',
        'description',
        'pathfile',
        'filename',
        'order',
        'created_at',
        'updated_at',
        'stat'
    ];

    public function question(){
        return $this->belongsTo('Module\\Quiz\\Models\\QuizModel', 'quiz_id', 'quiz_id');
    }
}
