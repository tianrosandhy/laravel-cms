<?php

namespace Module\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class QuizModel extends Model
{
    //
    protected $table = "maxvel_quiz";
    protected $primaryKey = 'quiz_id';

    protected $fillable = [
    	'quiz_id',
    	'question',
    	'slug',
    	'description',
    	'pathfile',
    	'filename',
        'bgdesktop_fileurl',
        'bgmobile_fileurl',
    	'sort',
        'created_at',
    	'updated_at',
    	'stat', 
    ];

    public function answers(){
        return $this->hasMany('Module\\Quiz\\Models\\QuizAnswerModel', 'quiz_id', 'quiz_id');
    }

}
