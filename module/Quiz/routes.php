<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('quiz', 'QuizController@index')->name('admin.quiz.index');
	Route::post('quiz', 'QuizController@store')->name('admin.quiz.store');
	Route::post('quiz/table', 'QuizController@getPost');
	Route::post('quiz/switch', 'QuizController@getSwitch');
	Route::post('quiz/edit', 'QuizController@edit')->name('admin.quiz.edit');
	Route::post('quiz/update/{id?}', 'QuizController@update')->name('admin.quiz.update');
	Route::post('quiz/destroy', 'QuizController@destroy')->name('admin.quiz.destroy');

});
