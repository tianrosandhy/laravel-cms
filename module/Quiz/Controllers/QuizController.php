<?php

namespace Module\Quiz\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use Module\Quiz\Models\QuizModel;
use Module\Quiz\Models\QuizAnswerModel;
use Module\Quiz\Traits\QuizTrait;
use Module\Quiz\Traits\QuizDatatable;
use DataTable;

class QuizController extends Controller
{
	public 	$request, 
			$model,
            $answer;
	
	use QuizTrait;
    use QuizDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
        $this->model = new QuizModel();
        $this->answer = new QuizAnswerModel();

        DataTable::set('columns', $this->columns);
        DataTable::set('model', $this->model);
        DataTable::set('data', $this->request->all());
	}

    public function index(){
    	return view('quiz::index')->with([
            'title' => 'Quiz',
            'table' => DataTable::tableGenerate([
                'server' => '/quiz/table',
                'hideOrder' => [4,5]
            ])
    	]);
    }

    public function store(){
    	$rule = [
    		'question' => 'required',
            'slug' => 'required',
            'answer' => 'required|array',
            'answer_desc' => 'array',
            'sort' => 'integer',
            'score' => 'required|array',
            'answer_order' => 'array'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

    	$cek = $this->model
    		->where('question', $this->request->question)
    		->where('stat', '<>', 9)
    		->get();

    	if(count($cek) > 0){
            return ajax_response('Data questionare dengan pertanyaan tersebut sudah ada');
    	}
    	else{
    		$param = $this->model->create([
    			'question' => $this->request->question,
                'slug' => $this->request->slug,
    			'description' => $this->request->description,
                'bgdesktop_fileurl' => $this->request->uploaded_img,
                'bgmobile_fileurl' => $this->request->uploaded_img2,
                'sort' => $this->request->sort,
                'stat' => 1
    		]);

            $lastid = $param->quiz_id;

            //save answers
            if(is_array($this->request->answer)){
                $n = count($this->request->answer);
                for($i=0; $i<$n; $i++){
                    if(strlen($this->request->answer[$i]) > 0){
                        $this->answer->create([
                            'answer' => $this->request->answer[$i],
                            'quiz_id' => $lastid,
                            'score' => intval($this->request->score[$i]),
                            'description' => $this->request->answer_desc[$i],
                            'order' => $this->request->answer_order[$i],
                            'stat' => 1
                        ]);
                    }
                }
            }

            return ajax_response('Berhasil menyimpan questionare', 'success');
    	}

    	return response()->json($out);
    }

    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = QuizModel::with('answers')->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
            'question' => 'required',
            'slug' => 'required',
            'answer' => 'required|array',
            'answer_desc' => 'array',
            'sort' => 'integer',
            'score' => 'required|array',
            'answer_order' => 'array'
        ];
        $validate = Validator::make($this->request->all(), $rule);
        if($validate->fails()){
            return ajax_response($validate->messages()->first());
        }

        $cek = $this->model
            ->where('question', $this->request->question)
            ->where('stat', '<>', 9)
            ->where('quiz_id', '<>', $id)
            ->get();

        if(count($cek) > 0){
            return ajax_response('Data questionare dengan pertanyaan tersebut sudah ada');
        }
        else{
            $instance = $this->model->find($id);
            $param = $instance->update([
                'question' => $this->request->question,
                'slug' => $this->request->slug,
                'description' => $this->request->description,
                'bgdesktop_fileurl' => $this->request->uploaded_img,
                'bgmobile_fileurl' => $this->request->uploaded_img2,
                'sort' => $this->request->sort,
                'stat' => 1
            ]);

            //delete all old answers
            $del = $this->answer->where('quiz_id', $id)->delete();

            //save answers
            $n = count($this->request->answer);
            for($i=0; $i<$n; $i++){
                if(strlen($this->request->answer[$i]) > 0){
                    $this->answer->create([
                        'answer' => $this->request->answer[$i],
                        'quiz_id' => $id,
                        'score' => intval($this->request->score[$i]),
                        'description' => $this->request->answer_desc[$i],
                        'order' => $this->request->answer_order[$i],
                        'stat' => 1
                    ]);
                }
            }

            return ajax_response('Berhasil mengupdate questionare', 'success');
        }

        return response()->json($out);
    }
    

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }
    
}
