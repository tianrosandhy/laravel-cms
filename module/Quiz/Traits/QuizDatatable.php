<?php
namespace Module\Quiz\Traits;

use DataTable;
use Module\Quiz\Models\QuizModel;
use Form;

trait QuizDatatable
{
	public $columns = [
		'Sort' => ['search' => true, 'col' => 'sort'],
		'Question' => ['search' => true, 'col' => 'question'],
		'Slug' => ['search' => true, 'col' => 'slug'],
		'Description' => ['search' => true, 'col' => 'description'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	
	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
		        $filtered['data'][] = [
					$row->sort,
					$row->question,
					$row->slug,
					$row->description,
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->quiz_id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						<a data-id="'.$row->quiz_id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#groupForm">
							<i class="fa fa-pencil"></i>
						</a>
						<a data-id="'.$row->quiz_id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>
					</div>'
				];

			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}