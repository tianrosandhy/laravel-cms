<?php
namespace Module\Quiz\Traits;

use Module\Quiz\Models\QuizModel;

trait QuizTrait
{

    public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new QuizModel();
			$data = $model->where('quiz_id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('quiz_id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status questionare";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Questionare tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}


}