	@section ('modal')
		<div id="groupModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					{{ Form::open(['method' => 'POST', 'class' => 'ajax-request']) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Add Questionnare</h3>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-3">
								Question
							</div>
							<div class="col-sm-9">
								<input type="text" name="question" class="form-control slug-toggle">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								Slug
							</div>
							<div class="col-sm-9">
								<input type="text" name="slug" class="form-control slug-target">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								Description
							</div>
							<div class="col-sm-9">
								<textarea name="description" class="form-control"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								Sort
							</div>
							<div class="col-sm-9">
								<input type="number" name="sort" class="form-control" min=0>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3">
								Desktop Image
							</div>
							<div class="col-sm-9">
								<div class="row">
									<div class="col-sm-6">
										<input type="hidden" name="uploaded_img" class="dropzone_uploaded">
										<div class="dropzone mydropzone dz-clickable" data-target="{{ url(env('ADMIN_PREFIX').'/api/gallery') }}">
											<div class="dz-default dz-message">
												<span>Drop files here to upload</span>
											</div>
										</div>										
									</div>
									<div class="col-sm-6 just-for-edit">
										<b>Uploaded Image</b>
										<br>
										<img src="" alt="" class="uploaded_desktop" style="width:50%">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3">
								Mobile Image
							</div>
							<div class="col-sm-9">
								<div class="row">
									<div class="col-sm-6">
										<input type="hidden" name="uploaded_img2" class="dropzone_uploaded2">
										<div class="dropzone mydropzone2 dz-clickable" data-target="{{ url(env('ADMIN_PREFIX').'/api/gallery') }}">
											<div class="dz-default dz-message">
												<span>Drop files here to upload</span>
											</div>
										</div>										
									</div>
									<div class="col-sm-6 just-for-edit">
										<b>Uploaded Image</b>
										<br>
										<img src="" alt="" class="uploaded_mobile" style="width:50%">
									</div>
								</div>
							</div>
						</div>


						<div class="answer-container">
							<div class="answers card">
								<div class="card-header">
									<div class="pull-right">
										<span class="btn btn-sm btn-danger remove-answer"><i class="fa fa-times"></i></span>
									</div>
								</div>
								<div class="card-block">

									<div class="row">
										<div class="col-sm-3">
											Answer
										</div>
										<div class="col-sm-9">
											<input type="text" name="answer[]" class="form-control">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											Score
										</div>
										<div class="col-sm-9">
											<input type="number" name="score[]" class="form-control" min=0>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											Order
										</div>
										<div class="col-sm-9">
											<input type="number" name="answer_order[]" class="form-control" min=0>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											Description
										</div>
										<div class="col-sm-9">
											<input type="text" name="answer_desc[]" class="form-control">
										</div>
									</div>
									
								</div>

							</div>
						</div>

						<div class="card">
							<a class="btn btn-primary add-answer-btn"><i class="fa fa-plus"></i> Add Answers</a>
						</div>


					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">
							<span class="fa fa-save"></span>
							Simpan
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	@endsection
