@extends('admin.inc.template')
@section('content')

	<h2>Questionnare</h2>

	@if(hasAccess('admin.quiz.store'))
	<a data-toggle="modal" data-target="#groupModal" class="btn btn-primary btn-add">
		<i class="fa fa-plus"></i>
		Add Questionare
	</a>
	@endif
	
	{!! $table !!}

	@include ('quiz::create')

@endsection


@push ('script')
<script>

	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'POST',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '/quiz/switch'
		}).done(function(dt){
			msghandling(dt);
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	});

	$("body").on("click", ".edit-btn", function(){
		var ini = $(this);
		post_id = ini.data('id');
		$(".just-for-edit").show();

		$(".ajax-request").attr('action', ADMIN_URL + '/quiz/update/'+post_id);

		$("#groupModal h3.modal-title").html('Update Data');

		if(post_id > 0){
			$.ajax({
				url : ADMIN_URL + '/quiz/edit',
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				var clone = $(".answer-container .card:first").clone(true);
				$(".answer-container").html('').append(clone);

				$.each(dt, function(nm, ctn){
					$("[name="+nm+"]").val(ctn);

					if(nm == 'bgdesktop_fileurl'){
						$("img.uploaded_desktop").attr('src', 'storage/thumb/thumb-500-'+ctn);
					}
					if(nm == 'bgmobile_fileurl'){
						$("img.uploaded_mobile").attr('src', 'storage/thumb/thumb-500-'+ctn);
					}

				});


				$.each(dt.answers, function(x, row){
					$("[name='answer[]']:last").val(row.answer);
					$("[name='score[]']:last").val(row.score);
					$("[name='answer_order[]']:last").val(row.order);
					$("[name='answer_desc[]']:last").val(row.description);

					var clone = $(".answer-container .card:first").clone(true);
					$(".answer-container").append(clone);
				});
				$(".answer-container .card:last").remove();

				$("#groupModal form").attr('action', ADMIN_URL + '/quiz/update/'+post_id);

				$("#groupModal").modal();
			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});
		}
	});

	$(".btn-add").on('click', function(){
		$("#groupModal h3.modal-title").html('Tambah Data Questionare');
		data = $("#groupModal form").serializeArray();
		$(".just-for-edit").hide();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name='"+data[dtt]['name']+"']").val("");
		}
		$(".ajax-request").attr('action', '');
		var clone = $(".answer-container .card:first").clone(true);
		$(".answer-container").html('').append(clone);
	});

	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "/quiz/destroy",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					swal('Error ' + dt.status, dt.responseText, 'warning');
				});

			}, function(){});
		}
	});


	$(".add-answer-btn").on('click', function(){
		var clone = $(".answer-container .card:first").clone(true);
		$(".answer-container").append(clone);
		$(".answer-container .card:last input").val('');
	});

	$("body").on('click', '.remove-answer', function(e){
		count = $(".answers.card").length;
		if(count > 1){
			//proses hapus current answer instance
			$(this).closest('.answers.card').remove();
		}
	});
</script>
@endpush