<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaxvelQuizAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('maxvel_quiz_answer')) {
            Schema::create('maxvel_quiz_answer', function (Blueprint $table) {
                $table->increments('answer_id');
                $table->string('answer',190);
                $table->integer('quiz_id');
                $table->integer('score')->nullable();
                $table->text('description')->nullable();
                $table->text('pathfile')->nullable();
                $table->text('filename')->nullable();
                $table->integer('order')->default(0);
                $table->timestamps();
                $table->tinyinteger('stat');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('maxvel_quiz_answer')) {
            Schema::dropIfExists('maxvel_quiz_answer');
        }
    }
}
