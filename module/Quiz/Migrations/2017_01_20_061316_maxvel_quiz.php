<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaxvelQuiz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('maxvel_quiz')) {
            Schema::create('maxvel_quiz', function (Blueprint $table) {
                $table->increments('quiz_id');
                $table->text('question');
                $table->text('slug');
                $table->text('description')->nullable();
                $table->text('pathfile')->nullable();
                $table->text('filename')->nullable();
                $table->text('bgdesktop_fileurl')->nullable();
                $table->text('bgmobile_fileurl')->nullable();
                $table->integer('sort')->default(0);
                $table->timestamps();
                $table->tinyInteger('stat');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('maxvel_quiz')) {
            Schema::dropIfExists('maxvel_quiz');
        }
    }
}
