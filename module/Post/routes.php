<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('post', 'PostController@index')->name('admin.post.index');
	Route::post('post', 'PostController@store')->name('admin.post.store');
	Route::post('post/table', 'PostController@getPost');
	Route::post('post/switch', 'PostController@getSwitch');
	Route::post('post/edit', 'PostController@edit')->name('admin.post.edit');
	Route::post('post/update/{id?}', 'PostController@update')->name('admin.post.update');
	Route::post('post/destroy', 'PostController@destroy')->name('admin.post.destroy');

	Route::get('post/list-post', 'PostController@listPost');

	
	Route::get('post-category', 'PostCategoryController@index')->name('admin.postcategory.index');
	Route::post('post-category', 'PostCategoryController@store')->name('admin.postcategory.store');
	Route::post('post-category/table', 'PostCategoryController@getPost');
	Route::post('post-category/switch', 'PostCategoryController@getSwitch');
	Route::post('post-category/edit', 'PostCategoryController@edit')->name('admin.postcategory.edit');
	Route::post('post-category/update/{id?}', 'PostCategoryController@update')->name('admin.postcategory.update');
	Route::post('post-category/destroy', 'PostCategoryController@destroy')->name('admin.postcategory.destroy');

	
});
