<?php
namespace Module\Post\Traits;

use DataTable;
use Module\Post\Models\PostModel;
use Form;

trait PostDatatable
{
	public $columns = [
		'Sort' => ['search' => true, 'col' => 'sort'],
		'Title' => ['search' => true, 'col' => 'title'],
		'Category' => ['search' => true, 'col' => 'category'],
		'Tags' => ['search' => true, 'col' => 'tags'],
		'Created At' => ['search' => true, 'col' => 'created_at'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	
	// Optional Class utk register automatic dynamic form
	public function registerForm(){
		$this->forms = [
			'Title' => Form::input('text', 'title', old('title', null), ['class' => 'form-control slug-toggle']),

			'Slug' => Form::input('text', 'slug', old('slug', null), ['class' => 'form-control slug-target']),

			'Category' => Form::select('category', get_list('cms_post_category', 'category_name'), old('category', null), ['class' => 'form-control']),

			'Tags' => Form::input('text', 'tags', old('tags', null), ['class' => 'form-control', 'data-role' => 'tagsinput']),

			'Content' => Form::textarea('content', old('content', null), ['class'=>'editor', 'id' => 'ckeditor']),

			'Sort No' => Form::input('number', 'sort', old('sort', null), ['class' => 'form-control']),

			'Image' => self::imageForm(),

			'Related To' => Form::input('text', 'related_to', old('related_to', null), ['id' => 'typeahead', 'class' => 'form-control']),
			'Status' => Form::select('stat', [1 => 'Live', 0 => 'Draft'], old('stat', null), ['class'=>'form-control']),
		];		
	}




	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$exp = explode(",", $row->tags);
				$tags = "";
				foreach($exp as $ex){
					$tags .= '<span class="label label-primary">'.trim($ex)."</span> ";
				}

				$btn = '';
				if(hasAccess('admin.post.update')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.post.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}


				$filtered['data'][] = [
					$row->sort,
					$row->title,
					DataTable::get_from_tb('cms_post_category',$row->category, 'category_name'),
					$tags,
					indo_date($row->created_at),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				];
			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}