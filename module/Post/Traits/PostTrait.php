<?php
namespace Module\Post\Traits;

use Form;

use Request;
use Module\Post\Models\PostModel;

use Module\Main\Http\Controllers\Api\DatatableApi;

trait PostTrait
{

    protected function imageForm($value=''){
		return view('admin.inc.partials.cropper-box', [
				'ratio' => [800, 500],
				'value' => $value
			])->render();    	
    }



    public function listPost(){
    	$sql = $this->model->where('stat','<>',9)->get()->pluck('title');
    	#biar output komanya dalam keadaan terescape
    	if(count($sql) > 0){
    		$final = [];
    		foreach($sql as $out){
    			$final[] = str_replace(',', "&#44;", $out);
    		}
    		return json_encode($final);
    	}
    	return [];
    }


    //Data Table Trait

    public function getPostTemp(){
    	$request = $this->request->all();

    	$dt = new DatatableApi($request, new PostModel(), $this->columns);
    	$output = $dt->generate(true);
    	$output['data'] = $this->table_format($request, $output);
    	//hapus query result mentah
    	unset($output['result']);

    	return json_encode($output);
    }

    public function getTitle($id){
    	$data = $this->model->find($id);
    	if(count($data) > 0)
    		return $data->title;
    	return null;
    }



	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new PostModel();
			$data = $model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}