<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_post', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->string('slug',190);
            $tb->integer('category');
            $tb->string('tags',190)->nullable();
            $tb->text('content')->nullable();
            $tb->string('img',190)->nullable();
            $tb->integer('sort')->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
            $tb->string('username',190);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_post');
    }
}
