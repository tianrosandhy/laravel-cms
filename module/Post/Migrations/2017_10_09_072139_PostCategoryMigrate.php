<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostCategoryMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_post_category', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('category_name',190);
            $tb->string('slug',190);
            $tb->text('description')->nullable();
            $tb->string('img',190)->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
            $tb->string('username',190);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_post_category');
    }
}
