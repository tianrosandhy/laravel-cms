@extends('admin.inc.template')
@section('content')
	
	@if(hasAccess('admin.postcategory.store'))
	<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
		Tambah Data
	</a>
	<br>
	@endif

	<hr>

	{!! $table !!}

	@section ('modal')
		@include ('post::create')
	@endsection
@stop

@push('script')
<script>

	//initialize ajax switchable
	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'POST',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '{{ $datatable_ajax['switch'] }}'
		}).done(function(dt){
			msghandling(dt);
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	});




	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-request").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-request").attr('action', '');
		$(".row.append").hide();

		form_refresh();
		blankCropper();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "{{ $datatable_ajax['edit'] }}",
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				form_refresh();
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

					if(nm == 'img'){
						$("[data-cropper-container]").replaceWith(ctn);
					}

					if(nm == 'stat'){
						if(ctn == 1){
							$("input#live").prop('checked', 'checked');
						}
						else{
							$("input#draft").prop('checked', 'checked');
						}
					}

					if(nm == 'tags'){
						$('[data-role="tagsinput"]').tagsinput('destroy');
						$('[data-role="tagsinput"]').tagsinput('refresh');
					}

					if(nm == 'related_post'){
						$("#typeahead").val(ctn);
						$("#typeahead").tokenfield('setTokens', ctn, false, false);
					}
				});

			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});

			$(".ajax-request").attr('action', '{{ $edit_action }}/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "{{ $datatable_ajax['delete'] }}",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					swal('Error ' + dt.status, dt.responseText, 'warning');
				});

			}, function(){});
		}
	});
</script>
@endpush