<?php

namespace Module\Post\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class PostCategoryModel extends Model
{
    //
    protected $table = "cms_post_category";
    protected $fillable = [
    	'id',
    	'category_name',
    	'slug',
    	'description',
    	'img',
    	'created_at',
    	'updated_at',
    	'stat', 
    	'username'
    ];

}
