<?php

namespace Module\Post\Models;

use Illuminate\Database\Eloquent\Model;

class PostRelated extends Model
{
    //
    protected $table = "cms_post_related";
    protected $fillable = [
    	'id',
    	'id_post',
    	'related_to',
    	'created_at',
    	'updated_at',
    ];

    //for datatable	

    public function get_post(){
        return $this->belongsto('Module\Post\PostModel', 'id', 'related_to');
    }

}
