<?php

namespace Module\Post\Models;

use Illuminate\Database\Eloquent\Model;

class PostModel extends Model
{
    //
    protected $table = "cms_post";
    protected $fillable = [
    	'id',
    	'title',
    	'slug',
    	'category',
    	'tags',
    	'content',
    	'img',
        'sort',
    	'created_at',
    	'updated_at',
    	'stat', 
    	'username'
    ];

    //for datatable	

    static function get_kategori(){
    	$sql = DB::table('cms_post_category')
    	->where('stat','<>',9)
    	->get();

    	$arr = ["" => ""];
    	foreach($sql as $row){
    		$arr[$row->id] = $row->category_name;
    	}
    	return $arr;
    }

    public function category(){
        return $this->belongsto('Module\Post\Models\PostCategoryModel');
    }

    public function related(){
        return $this->hasMany('Module\Post\Models\PostRelated', 'id_post', 'id');
    }
}
