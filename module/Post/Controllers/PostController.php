<?php

namespace Module\Post\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use Form;
use Validator;
use Auth;

use Module\Post\Models\PostModel;
use Module\Post\Models\PostRelated;
use Module\Post\Traits\PostTrait;



use DataTable;
use Module\Post\Traits\PostDatatable;

class PostController extends Controller
{
	public 	$request, 
			$forms,
			$model;
	
	use PostTrait;
	use PostDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new PostModel();

		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());

		$this->registerForm();
	}

    public function index(){
    	return view('post::index')->with([
    		'title' => 'Post',
    		'table' => DataTable::tableGenerate([
    			'server' => '/post/table', 
    			'custom' => [
	    			['category', 'cms_post_category', 'category_name', 'id'],
	    		],
	    		'hideOrder' => [5, 6],
	    		'sort' => [0, "asc"], //this is default ordering
    		]),

    		'forms' => $this->forms,
    		'listpost' => $this->listPost() //utk related post
    	]);

    }

    
    public function store(){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    		'category' => 'required|integer',
    		'content' => 'required',
    		'uploaded_img' => 'required'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data post dengan judul / slug tersebut sudah ada.");
		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$instance = $this->model->create([
				'title' => $this->request->title,
				'slug' => $slug,
				'category' => $this->request->category,
				'tags' => $this->request->tags,
				'content' => $this->request->content,
				'img' => $this->request->uploaded_img,
				'sort' => $this->request->sort,
				'created_at' => date("Y-m-d H:i:s"),
				'stat' => $this->request->stat,
				'username' => Auth::user()['username']
			]);

			//sebelum output sukses, simpan data related post jika ada
			$this->saveRelated($this->request->related_to, $instance->id);
			return ajax_response("Berhasil menyimpan post artikel", "success");
		}

    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    		'category' => 'required|integer',
    		'content' => 'required'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data post dengan judul / slug tersebut sudah ada.");
		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'title' => $this->request->title,
				'slug' => $slug,
				'category' => $this->request->category,
				'tags' => $this->request->tags,
				'content' => $this->request->content,
				'sort' => $this->request->sort,
				'stat' => $this->request->stat,
				'username' => Auth::user()['username']
			];

			if($this->request->uploaded_img){
				$list_update['img'] = $this->request->uploaded_img;
			}

			$this->model->where('id', $id)->update($list_update);

			$this->saveRelated($this->request->related_to, $id);

			return ajax_response("Berhasil mengupdate post artikel", "success");
		}
    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$model = new PostModel();
	    		$cek = $model->with('related')->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    			//out data related
	    			$out['related_post'] = [];
	    			foreach($cek->related as $row){
	    				$out['related_post'][] = $this->getTitle($row->related_to);
	    			}
	    			$out['related_post'] = implode(",",$out['related_post']);
	    			unset($out['related']);

	    			//bikin view upload file baru berdasarkan image data
	    			$out['img'] = self::imageForm($cek->img);
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    public function saveRelated($data, $save_to){
    	$title = explode(",", $data);
    	//truncate old related jika ada
    	PostRelated::where('id_post', $save_to)->delete();

    	//save process
    	foreach($title as $ttl){
    		$ttl = trim($ttl);
	    	$get = $this->model->where('title', $ttl)
	    		->where('stat','<>',9)
	    		->first();
	    	if(count($get) > 0){
	    		PostRelated::create([
	    			'id_post' => $save_to,
	    			'related_to' => $get->id
	    		]);
	    	}
    	}
    	return true;
    }

}
