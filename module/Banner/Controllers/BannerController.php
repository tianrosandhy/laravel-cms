<?php

namespace Module\Banner\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Module\Main\Http\Controllers\Api\DatatableApi;
use Validator;
use App\Model\GalleryModel;

use Module\Banner\Models\BannerModel;
use Module\Banner\Traits\BannerTrait;
use Module\Banner\Traits\BannerDatatable;
use DataTable;


class BannerController extends Controller
{
	public 	$request, 
			$forms,
			$model;
	
	use BannerTrait;
	use BannerDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new BannerModel();
		
		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());
		$this->registerForm();
	}

    public function index(){
    	return view('banner::index')->with([
    		'title' => 'Banner',
    		'forms' => $this->forms,
    		'datatable_ajax' => [
    			'tb' => '/banner/table',
    			'switch' => '/banner/switch',
    			'edit' => '/banner/edit',
    			'delete' => '/banner/destroy'
    		],
    		'table' => DataTable::tableGenerate([
    			'server' => '/banner/table',
    			'hideOrder' => [4,5]
    		])
    	]);

    }

    
    public function store(){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    		'uploaded_img' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
    		return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data banner dengan judul / slug tersebut sudah ada. Mohon gunakan judul lain agar mudah dibedakan");
		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$this->model->insert([
				'title' => $this->request->title,
				'slug' => $slug,
				'image' => $this->request->uploaded_img,
				'description' => $this->request->description,
				'ord' => intval($this->request->ord),
				'created_at' => date("Y-m-d H:i:s"),
				'stat' => intval($this->request->stat)
			]);
			return ajax_response("Berhasil menyimpan data banner", "success");
		}

    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response("Data banner dengan judul / slug tersebut sudah ada. Mohon gunakan judul lain agar mudah dibedakan");
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data post dengan judul / slug tersebut sudah ada.");
		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'title' => $this->request->title,
				'slug' => $slug,
				'description' => $this->request->description,
				'stat' => $this->request->stat,
			];

			if($this->request->uploaded_img){
				$list_update['image'] = $this->request->uploaded_img;
			}

			$this->model->where('id', $id)->update($list_update);
			return ajax_response("Berhasil mengupdate post artikel", "success");
		}

    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$delimg = $cek->image;
	    			GalleryModel::rollback($delimg);

		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    static function getByName($name,  $default=''){
    	//get Banner URL by name
    	$sql = BannerModel::where('title', $name)
    		->where('stat', 1)
    		->first();
    	if(count($sql) > 0){
    		return url('storage/'.$sql->image);
    	}
    	return $default;
    }
}
