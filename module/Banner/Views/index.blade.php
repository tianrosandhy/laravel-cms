@extends('admin.inc.template')
@section('content')

	@if(hasAccess('admin.banner.store'))
	<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
		Tambah Data
	</a>
	<br>
	@endif

	<hr>

	{!! $table !!}

	@include ('banner::create')

@stop

@push('script')
<script>	
	//initialize ajax switchable
	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'GET',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '/banner/switch'
		}).done(function(dt){
			msghandling(dt);
		});
	});




	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-request").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-request").attr('action', '');
		$(".append").hide();

		form_refresh();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "/banner/edit",
				type : 'GET',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				console.log(dt);
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

					if(nm == 'image'){
						$("[name='uploaded_img']").val('');
						$(".append").show();
						$(".append img").attr('src', 'storage/thumb/thumb-100-'+ctn);
					}

					if(nm == 'stat'){
						if(ctn == 1){
							$("input#live").prop('checked', 'checked');
						}
						else{
							$("input#draft").prop('checked', 'checked');
						}
					}
				});
				form_refresh();

			});

			$(".ajax-request").attr('action', ADMIN_URL + '/banner/update/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "/banner/destroy",
					type : 'GET',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				});

			}, function(){});
		}
	});

</script>
@endpush