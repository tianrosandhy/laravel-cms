<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('banner', 'BannerController@index')->name('admin.banner.index');
	Route::post('banner', 'BannerController@store')->name('admin.banner.store');
	Route::post('banner/table', 'BannerController@getPost');
	Route::get('banner/switch', 'BannerController@getSwitch');
	Route::get('banner/edit', 'BannerController@edit')->name('admin.banner.edit');
	Route::post('banner/update/{id?}', 'BannerController@update')->name('admin.banner.update');
	Route::get('banner/destroy', 'BannerController@destroy')->name('admin.banner.destroy');

});
