<?php

namespace Module\Banner\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class BannerModel extends Model
{
    //
    protected $table = "cms_banner";
    protected $fillable = [
    	'id',
    	'title',
    	'slug',
    	'image',
        'description',
        'ord',
    	'created_at',
    	'updated_at',
    	'stat'
    ];

}
