<?php 

namespace Module\Banner\Facade;

use Illuminate\Support\Facades\Facade;

class BannerFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Banner\\Controllers\\BannerController';
    }
}