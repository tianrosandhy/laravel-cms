<?php
namespace Module\Banner\Traits;

use DataTable;
use Module\Banner\Models\BannerModel;
use Form;

trait BannerDatatable
{
	public $columns = [
		'Title' => ['search' => true, 'col' => 'title'],
		'Image' => ['search' => false, 'col' => 'image'],
		'Description' => ['search' => true, 'col' => 'description'],
		'Image' => ['search' => false, 'col' => 'image'],
		'Created At' => ['search' => true, 'col' => 'created_at'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	
	// Optional Class utk register automatic dynamic form
	public function registerForm(){
		$this->forms = [
			'Title' => Form::input('text', 'title', old('title', null), ['class' => 'form-control slug-toggle']),
			'Slug' => Form::input('text', 'slug', old('slug', null), ['class' => 'form-control slug-target']),

			'Image Banner' => '
			<div class="row">
				<div class="col-sm-7">
					<input type="hidden" name="uploaded_img" value="" class="dropzone_uploaded">
					<div class="dropzone mydropzone" data-target="'.env('ADMIN_PREFIX').'/api/gallery"></div>
				</div>
				<div class="col-sm-5">
					<div class="append">
						<strong>Current Image Uploaded</strong>
						<br>
						<img src="">
					</div>
				</div>
			</div>',
			'Description' => Form::textarea('description', old('description', null), ['class' => 'editor',  'id' => 'editor']),
			'Status' => Form::select('stat', [1 => 'Live', 0 => 'Draft'], old('stat', null), ['class'=>'form-control']),
		];    	
	}




	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$btn = '';
				if(hasAccess('admin.banner.update')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.banner.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}

				$filtered['data'][] = array(
					$row->title,
					'<img height=100 src="'.url('storage/thumb/thumb-100-'.$row->image).'">',
					substr($row->description, 0, 150),
					indo_date($row->created_at),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				);


			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}