<?php
namespace Module\Banner\Traits;

use Module\Banner\Models\BannerModel;

trait BannerTrait
{

	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$data = $this->model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$this->model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}