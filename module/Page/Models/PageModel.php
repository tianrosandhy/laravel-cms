<?php

namespace Module\Page\Models;

use Illuminate\Database\Eloquent\Model;

class PageModel extends Model
{
    //
    protected $table = "cms_page";
    protected $fillable = [
    	'id',
    	'title',
    	'slug',
    	'tags',
    	'content',
    	'img',
    	'created_at',
    	'updated_at',
    	'stat', 
    ];

}
