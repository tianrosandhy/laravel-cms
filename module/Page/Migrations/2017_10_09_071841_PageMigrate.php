<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_page', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->string('slug',190);
            $tb->string('tags',190)->nullable();
            $tb->text('content')->nullable();
            $tb->string('img')->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_page');
    }
}
