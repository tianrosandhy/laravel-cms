<?php

namespace Module\Page\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\GalleryModel;

use Module\Page\Models\PageModel;
use Module\Page\Traits\PageTrait;
use Module\Page\Traits\PageDatatable;
use DataTable;


class PageController extends Controller
{
	public 	$request, 
			$forms,
			$model;
	
	use PageTrait;
	use PageDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new PageModel();
		
		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());
		$this->registerForm();
	}

    public function index(){
    	return view('page::index')->with([
    		'title' => 'Page',
    		'forms' => $this->forms,
    		'datatable_ajax' => [
    			'tb' => '/page/table',
    			'switch' => '/page/switch',
    			'edit' => '/page/edit',
    			'delete' => '/page/destroy'
    		],
    		'table' => DataTable::tableGenerate([
    			'server' => '/page/table',
    			'hideOrder' => [4,5]
    		])
    	]);

    }

    
    public function store(){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    		'content' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data page dengan judul / slug tersebut sudah ada.");
		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$this->model->insert([
				'title' => $this->request->title,
				'slug' => $slug,
				'tags' => $this->request->tags,
				'content' => $this->request->content,
				'img' => $this->request->uploaded_img,
				'created_at' => date("Y-m-d H:i:s"),
				'stat' => $this->request->stat,
			]);
			return ajax_response("Berhasil menyimpan page artikel", "success");
		}

    	return json_encode($out);

    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:255',
    		'slug' => 'required',
    		'content' => 'required'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data page dengan judul / slug tersebut sudah ada.");
		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'title' => $this->request->title,
				'slug' => $slug,
				'tags' => $this->request->tags,
				'content' => $this->request->content,
				'stat' => $this->request->stat,
			];

			if($this->request->uploaded_img){
				$list_update['img'] = $this->request->uploaded_img;
			}

			$this->model->where('id', $id)->update($list_update);
			return ajax_response("Berhasil mengupdate page artikel", "success");
		}

    	return json_encode($out);
    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$model = new PageModel();
	    		$cek = $model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

}
