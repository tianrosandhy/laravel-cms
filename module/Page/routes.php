<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('page', 'PageController@index')->name('admin.page.index');
	Route::post('page', 'PageController@store')->name('admin.page.store');
	Route::post('page/table', 'PageController@getPost');
	Route::post('page/switch', 'PageController@getSwitch');
	Route::post('page/edit', 'PageController@edit')->name('admin.page.edit');
	Route::post('page/update/{id?}', 'PageController@update')->name('admin.page.update');
	Route::post('page/destroy', 'PageController@destroy')->name('admin.page.destroy');
});
