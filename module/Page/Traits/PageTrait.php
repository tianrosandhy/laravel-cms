<?php
namespace Module\Page\Traits;

use Form;
use Request;
use Module\Page\Models\PageModel;

trait PageTrait
{

	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new PageModel();
			$data = $model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Page tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}