<?php
$router->group(['prefix' => env('ADMIN_PREFIX').'/product'], function($route){
	Route::get('/', 'ProductController@index')->name('admin.product.index');
	Route::post('/', 'ProductController@store')->name('admin.product.store');
	Route::post('/table', 'ProductController@getPost');
	Route::post('/switch', 'ProductController@getSwitch');
	Route::post('/edit', 'ProductController@edit')->name('admin.product.edit');
	Route::post('/update/{id?}', 'ProductController@update')->name('admin.product.update');
	Route::post('/destroy', 'ProductController@destroy')->name('admin.product.destroy');
});


$router->group(['prefix' => env('ADMIN_PREFIX').'/product-category'], function($route){
	Route::get('/', 'ProductCategoryController@index')->name('admin.productcategory.index');
	Route::post('/', 'ProductCategoryController@store')->name('admin.productcategory.store');
	Route::post('/table', 'ProductCategoryController@getPost');
	Route::post('/switch', 'ProductCategoryController@getSwitch');
	Route::post('/edit', 'ProductCategoryController@edit')->name('admin.productcategory.edit');
	Route::post('/update/{id?}', 'ProductCategoryController@update')->name('admin.productcategory.update');
	Route::post('/destroy', 'ProductCategoryController@destroy')->name('admin.productcategory.destroy');
});
