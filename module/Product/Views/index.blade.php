@extends('admin.inc.template')
@section('content')

	<h3>{{ $title }}</h3>

	@if(hasAccess('admin.product.store'))
	<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
		Tambah Data
	</a>
	<br>
	@endif

	<hr>

	{!! $table !!}

	@include ('product::create')

@stop

@push ('scripts')
<script>

	//initialize ajax switchable
	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'POST',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '/product/switch'
		}).done(function(dt){
			msghandling(dt);
		}).fail(function(dt){
			unauthorizeModal(dt);
		});
	});




	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-request").serializeArray();
		form_refresh();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val('');
		}
		$(".ajax-request").attr('action', '');
		$(".row.append").hide();

		$(".multi-image-holder").html('');

	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "/product/edit",
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				form_refresh();
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

					if(nm == 'img'){
						$(".row.append").show();
						$(".row.append img").attr('src', 'storage/thumb/thumb-100-'+ctn);
					}

					if(nm == 'stat'){
						if(ctn == 1){
							$("input#live").prop('checked', 'checked');
						}
						else{
							$("input#draft").prop('checked', 'checked');
						}
					}

					if(nm == 'imageData'){
						$(".multiple-input-instance").html(ctn);
					}
				});

			}).fail(function(dt){
				unauthorizeModal(dt);
			});

			$(".ajax-request").attr('action', ADMIN_URL + '/product/update/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "/product/destroy",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					sweet_alert(dt.type, dt.message);
					tb_data.ajax.reload();
				}).fail(function(dt){
					unauthorizeModal(dt);
				});

			}, function(){});
		}
	});

</script>
@endpush