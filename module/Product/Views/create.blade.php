<div id="modalForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{{ Form::open(['method' => 'POST', 'class' => 'ajax-request']) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Tambah Data Produk</h3>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label>Product Title</label>
							<input type="text" class="form-control slug-toggle" name="nama">
						</div>
						
						<div class="form-group">
							<label>Product Slug</label>
							<input type="text" class="form-control slug-target" name="slug">
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control" name="description"></textarea>
						</div>
						
						<div class="form-group">
							<label>Category</label>
							{!! DataTable::customComboBox('category', 'cms_product_category', 'category_name', 'id', [], false) !!}
						</div>
						
						<!-- Configurable Field -->

						@foreach(config('product') as $data)
						<div class="form-group">
							<div class="row" title="{{ $data['label'] }}">
								<div class="col-sm-3">
									<label>{{ $data['label'] }}</label>
								</div>
								<div class="col-sm-9">
								@if($data['type'] == 'select')
									{!! Form::select($data['name'], $data['list'], (isset($filled[$data['name']]) ? $filled[$data['name']] : null), $data['attr']) !!}
								@elseif($data['type'] == 'textarea')
									{!! Form::textarea($data['name'], (isset($filled[$data['name']]) ? $filled[$data['name']] : null), $data['attr']) !!}
								@elseif($data['type'] == 'file')
									@include ('admin.inc.partials.cropper-box', [
										'ratio' => isset($data['ratio']) ? $data['ratio'] : [800, 600],
										'value' => (isset($filled[$data['name']]) ? $filled[$data['name']] : ''),
										'name' => $data['name'],
									])
								@else
									{{ Form::input($data['type'], $data['name'], (isset($filled[$data['name']]) ? $filled[$data['name']] : null), $data['attr']) }}
								@endif
								</div>
							</div>
						</div>
						@endforeach
						

						<div class="form-group">
							<label>Status</label>
							<select name="stat" class="form-control">
								<option value="1">Live</option>
								<option value="0">Draft</option>
							</select>
						</div>

					</div>
					<div class="col-sm-4 multiple-input-instance">
						@include ('admin.inc.multiple-input')
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span>
					Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@push ('script')
{!! register_single('assets/plugins/jquery-autonumeric/autoNumeric.js', 'js') !!}
<script>
$(function(){
	$('.autonumeric').autoNumeric(
		'init', 
		{
			mDec: '0'
		}
	);
});
</script>
@endpush