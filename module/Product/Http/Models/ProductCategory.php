<?php

namespace Module\Product\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table = "cms_product_category";
    protected $fillable = [
    	'id',
    	'category_name',
    	'slug',
    	'description',
    	'img',
    	'created_at',
    	'updated_at',
    	'stat', 
    	'username'
    ];

   	public function product(){
        return $this->hasMany('Module\Product\Http\Models\ProductModel', 'category');
    }


}
