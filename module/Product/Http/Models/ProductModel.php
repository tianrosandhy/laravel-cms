<?php
namespace Module\Product\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    //
    protected $table = "cms_product";
    protected $fillable = [
    	'nama',
    	'slug',
    	'description',
        'category',
    	'stat'
    ];


    public function meta(){
    	return $this->hasMany('Module\Product\Http\Models\ProductMeta', 'id_product');
    }    

    public function images(){
    	return $this->hasMany('Module\Product\Http\Models\ProductImage', 'id_product');
    }    

    public function getCategory(){
        return $this->belongsTo('Module\Product\Http\Models\ProductCategory', 'category');
    }

}
