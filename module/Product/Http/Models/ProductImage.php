<?php
namespace Module\Product\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    protected $table = "cms_product_image";
    protected $fillable = [
    	'id_product',
    	'filename',
    	'filepath',
    	'stat'
    ];


    public function product(){
    	return $this->belongsTo('Module\Product\Http\Models\ProductModel', 'id_product');
    }
    
}
