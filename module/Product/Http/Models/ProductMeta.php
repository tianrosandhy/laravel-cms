<?php
namespace Module\Product\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductMeta extends Model
{
    //
    protected $table = "cms_product_meta";
    protected $fillable = [
    	'id_product',
    	'param',
    	'value',
    	'stat'
    ];


    public function product(){
    	return $this->belongsTo('Module\Product\Http\Models\ProductModel', 'id_product');
    }    

   
    
}
