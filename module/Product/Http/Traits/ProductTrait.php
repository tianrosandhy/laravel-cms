<?php
namespace Module\Product\Http\Traits;

use Form;
use Module\Product\Http\Models\ProductModel;
use Module\Product\Http\Models\ProductMeta;
use Module\Product\Http\Models\ProductImage;

use Module\Main\Http\Controllers\Api\DatatableApi;

trait ProductTrait
{
	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new ProductModel();
			$data = $model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Product tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}




	protected function storeMeta($instance){
		ProductMeta::where('id_product', $instance->id)->delete(); //delete old meta product data

		//get meta from config
		foreach(config('product') as $row){
			ProductMeta::create([
				'id_product' => $instance->id,
				'param' => $row['name'],
				'value' => $this->request->{$row['name']}
			]);
		}
		return true;
	}

	protected function storeProductImage($instance){
		ProductImage::where('id_product', $instance->id)->delete(); // delete old product image data

		$uploaded = $this->request->uploaded_img;
		if(strlen($uploaded) > 0){
			$pecah = explode('&&', $uploaded);
			foreach($pecah as $filename){
				ProductImage::create([
					'id_product' => $instance->id,
					'filename' => $filename,
					'filepath' => \Storage::url($filename)
				]);
			}
		}
		return true;
	}
}