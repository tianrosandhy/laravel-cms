<?php
namespace Module\Product\Http\Traits;

use DataTable;
use Module\Product\Http\Models\ProductCategory;
use Form;

trait ProductCategoryDatatable
{
	public $columns = [
		'Category' => ['search' => true, 'col' => 'category_name'],
		'Slug' => ['search' => true, 'col' => 'slug'],
		'Description' => ['search' => true, 'col' => 'description'],
		'Created At' => ['search' => true, 'col' => 'created_at'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	
	// Optional Class utk register automatic dynamic form
    public function registerForm(){
		$this->forms = [
			'Category Name' => Form::input('text', 'category_name', old('category_name', null), ['class' => 'form-control slug-toggle']),

			'Slug' => Form::input('text', 'slug', old('slug', null), ['class' => 'form-control slug-target']),

			'Description' => Form::textarea('description', old('description', null), ['class'=>'editor', 'id' => 'editor']),

			'Status' => Form::select('stat', [1 => 'Live', 0 => 'Draft'], old('stat', null), ['class'=>'form-control']),
		];    	
    }




	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$btn = '';
				if(hasAccess('admin.postcategory.update')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.postcategory.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}


				$filtered['data'][] = array(
					$row->category_name,
					$row->slug,
					$row->description,
					indo_date($row->created_at),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				);
			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}