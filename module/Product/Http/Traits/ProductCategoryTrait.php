<?php
namespace Module\Product\Http\Traits;

use Form;
use Module\Product\Models\ProductCategory;

trait ProductCategoryTrait
{

	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new ProductCategory();
			$data = $model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Product tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}