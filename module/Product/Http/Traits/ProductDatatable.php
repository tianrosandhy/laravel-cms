<?php
namespace Module\Product\Http\Traits;

use DataTable;
use Module\Product\Models\ProductModel;
use Form;

trait ProductDatatable
{
	public $columns = [
		'Product Title' => ['search' => true, 'col' => 'nama'],
		'Slug' => ['search' => true, 'col' => 'slug'],
		'Description' => ['search' => true, 'col' => 'description'],
		'Category' => ['search' => true, 'col' => 'category', 'custom' => true],
		'Created At' => ['search' => true, 'col' => 'created_at'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	
	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
		        $btn = ''; 
		        if(hasAccess('admin.product.update')){ 
		          $btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm"> 
		              <i class="fa fa-pencil"></i> 
		            </a>'; 
		        } 

		        if(hasAccess('admin.product.destroy')){ 
		          $btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn"> 
		              <i class="fa fa-trash"></i> 
		            </a>'; 
		        } 
		 
		        $filtered['data'][] = array( 
		          $row->nama, 
		          $row->slug, 
		          substr(strip_tags($row->description), 0, 150).'...', 
		          (isset($row->getCategory->category_name) ? $row->getCategory->category_name : ''),
		          indo_date($row->created_at), 
		          '<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>', 
		          '<div class="btn-group"> 
		            '.$btn.' 
		          </div>' 
		        ); 

			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}