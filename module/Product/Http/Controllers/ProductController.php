<?php

namespace Module\Product\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use Module\Product\Http\Models\ProductModel;
use Module\Product\Http\Traits\ProductTrait;
use Module\Product\Http\Traits\ProductDatatable;
use DataTable;

class ProductController extends Controller
{
	public 	$request, 
			$forms,
			$model;
	
	use ProductTrait;
	use ProductDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = ProductModel::with('getCategory');
		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());
	}

    public function index(){
    	return view('product::index')->with([
    		'title' => 'Product',
    		'table' => DataTable::tableGenerate([
    			'server' => '/product/table',
    			'custom' => [
    				['category', 'cms_product_category', 'category_name', 'id']
    			],
    			'hideOrder' => [4,5]
    		])
    	]);

    }

    
    public function store(){
    	$rule = [
    		'nama' => 'required|max:255',
    		'slug' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
    		return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('nama', $this->request->nama)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data product dengan nama / slug tersebut sudah ada.");
		}
		else{
            $stat = strlen($this->request->stat) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->nama);
			//proses simpan
			$instance = $this->model->create([
				'nama' => $this->request->nama,
				'slug' => $slug,
				'category' => $this->request->category,
				'description' => $this->request->description,
				'stat' => intval($this->request->stat),
			]);
			
			self::storeMeta($instance);
			self::storeProductImage($instance);

			return ajax_response('Product data has been saved', 'success');
		}

    }

    public function update($id){
    	$rule = [
    		'nama' => 'required|max:255',
    		'slug' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('nama', $this->request->nama)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data product dengan nama / slug tersebut sudah ada.");
		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->nama);
			//proses simpan
			$list_update = [
				'nama' => $this->request->nama,
				'slug' => $slug,
				'category' => $this->request->category,
				'description' => $this->request->description,
				'stat' => intval($this->request->stat),
			];

			$instance = ProductModel::with('getCategory')->findOrFail($id);
			$instance->update($list_update);

			self::storeMeta($instance);
			self::storeProductImage($instance);

			return ajax_response('Product data has been updated', 'success');
		}

    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$model = $this->model->with('meta', 'images');
	    		$cek = $model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    			//normalize meta
	    			if(count($cek->meta) > 0){
		    			$meta = $cek->meta->pluck('value', 'param')->toArray();
		    			$out = $out + $meta;
	    			}

	    			//manage images relation
	    			if(count($cek->images) > 0){
		    			$oldimage = $cek->images->pluck('filename')->toArray();
		    			$images = view('admin.inc.multiple-input', compact('oldimage'))->render();

		    			$out['imageData'] = $images;
	    			}
	    			else{
	    				$out['imageData'] = view('admin.inc.multiple-input')->render();
	    			}

	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->with('meta', 'images')->find($id);
	    		if($cek){
		    		//hapus meta dan image juga
					$cek->stat = 9;
					$cek->save();	
		    		
		    		return ajax_response('Product data has been deleted', 'success');
	    		}
	    		else{
	    			return ajax_response("Data not found");
	    		}
	    	}
    	}

		return ajax_response('Invalid operation');
    }

}
