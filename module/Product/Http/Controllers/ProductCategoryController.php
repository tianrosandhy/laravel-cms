<?php

namespace Module\Product\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Form;
use Validator;
use App\Model\GalleryModel;

use Module\Product\Http\Models\ProductCategory;
use Module\Product\Http\Traits\ProductCategoryTrait;

use DataTable;
use Module\Product\Http\Traits\ProductCategoryDatatable;


class ProductCategoryController extends Controller{
	public 	$request,
			$forms,
			$model;

	use ProductCategoryTrait;
	use ProductCategoryDatatable;

	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new ProductCategory();

		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());

		$this->registerForm();
	}


	public function index(){
    	return view('product::index-category')->with([
    		'title' => 'Product Category',
    		'forms' => $this->forms,
    		'datatable_ajax' => [
    			'switch' => '/product-category/switch',
    			'edit' => '/product-category/edit',
    			'delete' => '/product-category/destroy'
    		],
    		'edit_action' => route('admin.productcategory.update'),

    		'table' => DataTable::tableGenerate([
    			'server' => '/product-category/table',
    			'hideOrder' => [4,5],
    		]),
    	]);
    }


    public function store(){
    	
    	$rule = [
    		'category_name' => 'required|max:80',
    		'slug' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('category_name', $this->request->title)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data product category dengan judul / slug tersebut sudah ada.";
		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$this->model->insert([
				'category_name' => $this->request->category_name,
				'slug' => $slug,
				'description' => $this->request->description,
				'created_at' => date("Y-m-d H:i:s"),
				'stat' => $this->request->stat,
				'username' => Auth::user()['username']
			]);
			$out['success'] = "Berhasil menyimpan product kategori artikel";
		}

    	return json_encode($out);

    }

    public function update($id){
    	$rule = [
    		'category_name' => 'required|max:80',
    		'slug' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('category_name', $this->request->category_name)
			->where('slug', $this->request->slug)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data product category dengan judul / slug tersebut sudah ada.";

		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'category_name' => $this->request->category_name,
				'slug' => $slug,
				'description' => $this->request->description,
				'stat' => $this->request->stat,
				'username' => Auth::user()['username']
			];

			$this->model->where('id', $id)->update($list_update);
			$out['success'] = "Berhasil mengupdate product artikel";
		}

    	return json_encode($out);
    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }
}
