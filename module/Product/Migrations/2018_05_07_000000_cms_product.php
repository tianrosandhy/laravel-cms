<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cms_product')) {
            Schema::create('cms_product', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nama');
                $table->string('slug');
                $table->text('description')->nullable();
                $table->integer('category');
                $table->timestamps();
                $table->tinyinteger('stat');
            });
        }

        if (!Schema::hasTable('cms_product_meta')) {
            Schema::create('cms_product_meta', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('id_product');
                $table->string('param');
                $table->text('value')->nullable();
                $table->timestamps();
                $table->tinyinteger('stat')->default(1);
            });
        }

        if (!Schema::hasTable('cms_product_image')) {
            Schema::create('cms_product_image', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('id_product');
                $table->text('filename');
                $table->text('filepath')->nullable();
                $table->timestamps();
                $table->tinyinteger('stat')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cms_product')) {
            Schema::dropIfExists('cms_product');
        }

        if (Schema::hasTable('cms_product_meta')) {
            Schema::dropIfExists('cms_product_meta');
        }

        if (Schema::hasTable('cms_product_image')) {
            Schema::dropIfExists('cms_product_image');
        }
    }
}
