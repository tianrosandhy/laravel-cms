<?php
namespace Module\Language\Http\Traits;

use DataTable;
use Module\Post\Models\LangModel;
use Form;

trait LangDatatable
{
	public $columns = [
		'Group' => ['search' => true, 'col' => 'group', 'custom' => true],
		'Parameter' => ['search' => true, 'col' => 'param'],
		'Content' => ['search' => true, 'col' => 'content'],
		'Content Translated' => ['search' => true, 'col' => 'content_trans'],
		'Status'	=> ['search' => false, 'col' => 'stat'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$btn = '';
				if(hasAccess('admin.lang.update')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.lang.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}


				$filtered['data'][] = array(
					'<span class="label label-primary">'.config('lang.group')[$row->group].'</span>',
					'<strong>'.$row->param.'</strong>',
					descriptionMaker($row->content),
					descriptionMaker($row->content_trans),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				);
			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}