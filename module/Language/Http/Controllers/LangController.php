<?php
namespace Module\Language\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Form;
use Validator;
use Auth;

use Module\Language\Http\Models\LanguageModel;

use DataTable;
use Module\Language\Http\Traits\LangDatatable;


class LangController extends Controller{
	public 	$request,
			$forms,
			$model;

	use LangDatatable;

	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new LanguageModel();

		DataTable::set('columns', $this->columns);
		DataTable::set('model', $this->model);
		DataTable::set('data', $this->request->all());
	}


	public function index(){
    	return view('lang::index')->with([
    		'title' => 'Language Data',
    		'forms' => $this->forms,
    		'table' => DataTable::tableGenerate([
    			'server' => '/lang/table',
    			'hideOrder' => [4,5],
    			'customManual' => [
    				['group', config('lang.group')]
    			]
    		]),
    	]);
    }


    public function store(){
    	$rule = [
    		'param' => 'required|max:80',
    		'group' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
    		return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('param', $this->request->param)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data language dengan parameter tersebut sudah ada.");
		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 

			$this->model->insert([
				'param' => $this->request->param,
				'group' => $this->request->group,
				'content' => $this->request->content,
				'content_trans' => $this->request->content_trans,
				'stat' => $this->request->stat,
			]);
			return ajax_response("Berhasil menyimpan language", "success");
		}

    }

    public function update($id){
    	$rule = [
    		'param' => 'required|max:80',
    		'group' => 'required',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
    		return ajax_response($validate->messages()->first());
    	}


		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('param', $this->request->param)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data language dengan judul / slug tersebut sudah ada.");
		}
		else{

			//proses simpan
			$list_update = [
				'param' => $this->request->param,
				'group' => $this->request->group,
				'content' => $this->request->content,
				'content_trans' => $this->request->content_trans,
				'stat' => $this->request->stat,
			];

			$this->model->where('id', $id)->update($list_update);
			return ajax_response("Berhasil mengupdate data language", "success");
		}

    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }



    //switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$data = $this->model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$this->model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status language";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}
