<?php
namespace Module\Language\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageModel extends Model
{
    //
    protected $table = "cms_lang";
    protected $fillable = [
        'id',
        'param',
        'group',
        'content',
        'content_trans',
        'stat'
    ];

}
