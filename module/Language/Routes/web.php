<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->get('lang', 'LangController@index')->name('admin.lang.index');
	$route->post('lang', 'LangController@store')->name('admin.lang.store');
	$route->post('lang/table', 'LangController@getPost');
	$route->post('lang/switch', 'LangController@getSwitch');
	$route->post('lang/edit', 'LangController@edit')->name('admin.lang.edit');
	$route->post('lang/update/{id?}', 'LangController@update')->name('admin.lang.update');
	$route->post('lang/destroy', 'LangController@destroy')->name('admin.lang.destroy');
});
