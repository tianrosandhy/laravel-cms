<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Langmigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cms_lang')) {
            Schema::create('cms_lang', function (Blueprint $table) {
                $table->increments('id');
                $table->string('param');
                $table->string('group')->nullable();
                $table->text('content');
                $table->text('content_trans');
                $table->timestamps();
                $table->tinyinteger('stat');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cms_lang')) {
            Schema::dropIfExists('cms_lang');
        }
    }
}
