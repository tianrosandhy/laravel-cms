<div id="modalForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{{ Form::open(['method' => 'POST', 'class' => 'ajax-request']) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Tambah Data Language</h3>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Group</label>
							{!! DataTable::manualComboBox('group', config('lang.group'), false) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Parameter Name</label>
							<input type="text" class="form-control" name="param">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Content</label>
							<textarea name="content" class="form-control"></textarea>
						</div>						
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label>Content Translated</label>
							<textarea name="content_trans" class="form-control"></textarea>
						</div>
					</div>
				</div>



				<div class="form-group">
					<label>Status</label>
					<select name="stat" class="form-control">
						<option value="1">Live</option>
						<option value="0">Draft</option>
					</select>
				</div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span>
					Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>