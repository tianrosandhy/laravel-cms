<?php
namespace Module\Main;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\Router;

use Module\Main\Services\Cache;
use Module\Main\Services\MailHelper;
use Module\Main\Services\PasswordChangerService;
use Module\Main\Services\DataTable;

class ModuleMainProvider extends ServiceProvider
{
	protected $namespace = 'Module\Main\Http\Controllers';

	public function boot(){
		$this->loadMigrationsFrom(realpath(__DIR__."/Migrations"));
	}

	protected function mapping(Router $router){
		$router->group(['namespace' => $this->namespace, 'middleware' => 'web'], function($router){
			require realpath(__DIR__."/Routes/web.php");
			require realpath(__DIR__."/Routes/api.php");
		});
	}

	public function register(){
		$this->mapping($this->app->router);
		$this->loadViewsFrom(realpath(__DIR__."/Views"), 'main');

        //register cache facade
        $this->app->singleton('cache-facade', function ($app) {
            return new Cache($app);
        });

        //register cache facade
        $this->app->singleton('mailhelper-facade', function ($app) {
            return new MailHelper($app);
        });

		$this->app->singleton('password-changer-facade', function ($app) {
            return new PasswordChangerService($app);
        });

        $this->app->singleton('datatable-facade', function ($app) {
            return new DataTable($app);
        });
	}
}