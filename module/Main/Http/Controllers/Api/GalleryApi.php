<?php

namespace Module\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Image;
use App\Model\GalleryModel;
use Storage;

class GalleryApi extends Controller
{
    //

    public function index(Request $request){
    	$upload = new GalleryModel([100,300,500]);

    	$proses = $upload->upload($request->file('file'));

    	if($proses){
    		$out = $upload->filename;
    	}
    	else{
    		//error
    		$out = false;
    	}

    	return $out;
    }

    public function ckeditor(Request $req){
        $upload = new GalleryModel([100,300,500]);

        $proses = $upload->upload($req->file('upload'));

        if($proses){
            $out = $upload->filename;
        }
        else{
            //error
            $out = false;
        }

        $final_url = Storage::url($out);
        $funcNum = $_GET['CKEditorFuncNum'] ;
        $CKEditor = $_GET['CKEditor'] ;
        $langCode = $_GET['langCode'] ;
        $token = $_POST['ckCsrfToken'] ;

        $message = 'The uploaded file has been uploaded';

        return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$final_url', '$message');</script>";
    }

    
}
