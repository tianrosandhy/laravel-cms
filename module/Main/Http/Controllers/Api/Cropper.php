<?php

namespace Module\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Image;
use App\Model\GalleryModel;
use Storage;

class Cropper extends Controller
{
    //

    public function index(Request $request){
        $data = $request->image;

        list($type, $data) = explode(';', $data);

        //cropped image in JPG only
        if(strpos($type, 'jpg') !== false || strpos($type, 'jpeg') !== false){
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $imageName = time().'-'.md5(uniqid()).'.jpg';
            Storage::put($imageName, $data);

            //resize after upload
            $gall = new GalleryModel([100,500]);
            $gall->filename = $imageName;
            $gall->resize(false);


            $html = '
            <div class="image-holder">
                <img src="'.url('storage/thumb/thumb-100-'.$imageName).'" alt="User uploaded image">
                <br>
                <span class="close-button remove-upload" data-hash="'.$imageName.'">
                    x
                </span>
            </div>
            ';

            return [
                'name' => $imageName,
                'html' => $html
            ];
        }
        return ['name' => '', 'html' => ''];
    }



    public function remove(Request $request){
        $validate = \Validator::make($request->all(), [
            'filename' => 'required'
        ]);
        if($validate->fails()){
            return [
                'type' => 'error',
                'message' => $validate->messages()->first()
            ];
        }

        //rollback file if exists
        GalleryModel::rollback($request->filename);
        return [
            'type' => 'success',
            'message' => 'Berhasil menghapus file gambar'
        ];
    }

    
}
