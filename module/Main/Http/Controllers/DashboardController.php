<?php
namespace Module\Main\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DataTableModel;

use Validator;
use Auth;
use PasswordChanger;
use Module\Main\Http\Models\UserPasswordStore;
use App\User;

class DashboardController extends Controller
{

	public 	$request,
			$columns,
			$model,
            $regex = '/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,32}$/';

	public function __construct(Request $req){
		$this->middleware('login');
		$this->request = $req;
        $this->model = new UserPasswordStore();
	}

	public function index(){
    	return view('admin.dashboard')->with([
    		
    	]);
    }

    public function login(){
        if(Auth::user()){
           return redirect()->route('admin.dashboard'); 
        }
    	return view('admin.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.dashboard')->with([
            'success' => 'Anda sudah logout dari sistem'
        ]);
    }



    public function updatePassword(){
        $cek = PasswordChanger::passwordUpdated();
        if(strlen($cek) > 0){
            $data = PasswordChanger::getUserData();
            $msg = count($data) > 0 ? 'We detect that the last password you\'ve made is at '.date('d F Y H:i:s', strtotime($data->created_at)) : 'We detect that you have <b>never update password yet</b>.';

            return view('main::update-password')->with([
                'breadcrumbs' => [
                    'Update Password' => admin_url('update-password')
                ],
                'withForcePasswordRedirect' => true,
                'data' => $data,
                'msg' => $msg
            ]);
        }

        return redirect()->url('/');
    }

    public function postUpdatePassword(){
        $current_user = Auth::user();
        $validate = Validator::make($this->request->all(), [
            'new_password' => 'required|min:8|confirmed|regex:'.$this->regex
        ], [
            'new_password.required' => 'Password is required',
            'new_password.min' => 'Please make password at least 8 character',
            'new_password.confirmed' => 'The password and confirmation box is mismatch',
            'new_password.regex' => 'Please use password with minimal 1 lowercase character, 1 uppercase character, 1 number, and 1 symbol'
        ])->validate();


        //cek password lama
        $hash = $this->request->new_password;
        if($this->model->hashed){
            $hash = sha1($this->request->new_password);
        }

        $cek = $this->model->where('user_id', $current_user->id)->orderBy('created_at', 'DESC')->take($this->model->rememberLimit)->get();

        foreach($cek as $roww){
            if($roww->password_hash == $hash){
                return back()->withErrors([
                    'password' => 'Please input password that you never used before. (The password inputted last used is '.date('d F Y', strtotime($roww->created_at)).')'
                ]);                
            }
        }


        //proses storing password baru
        $newpass = bcrypt($this->request->new_password);
        User::where('id', $current_user->id)
            ->update([
                'password' => $newpass
            ]);

        $this->model->create([
            'user_id' => $current_user->id,
            'password_hash' => $hash
        ]);

        return redirect(admin_url('/'));
    }


}