<?php
namespace Module\Main\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserPasswordStore extends Model
{
    //custom property utk atur password yg disimpan dihash atau tidak
    public $hashed = true; 
    public $resetTime = 2; //in months
    public $rememberLimit = 10;

    protected $table = "cms_password_store";
    protected $fillable = [
    	'user_id',
    	'password_hash', //bisa dipakai store password mentah juga
    	'stat'
    ];


    
}
