@extends ('main::mail.partials.template')
@section ('content')

<!-- Hero Image, Flush : BEGIN -->
<tr>
    <td style="background-color: #ffffff;">
        <img src="http://placehold.it/1200x600" width="600" height="" alt="alt_text" border="0" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555; margin: auto;" class="g-img">
    </td>
</tr>
<!-- Hero Image, Flush : END -->

<!-- 1 Column Text + Button : BEGIN -->
<tr>
    <td style="background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    <h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #333333; font-weight: normal;">Praesent laoreet malesuada&nbsp;cursus.</h1>
                    <p style="margin: 0 0 10px;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent laoreet malesuada cursus. Maecenas scelerisque congue eros eu posuere. Praesent in felis ut velit pretium lobortis rhoncus ut&nbsp;erat.</p>
                    <ul style="padding: 0; margin: 0; list-style-type: disc;">
						<li style="margin:0 0 10px 20px;" class="list-item-first">A list item.</li>
						<li style="margin:0 0 10px 20px;">Another list item here.</li>
						<li style="margin: 0 0 0 20px;" class="list-item-last">Everyone gets a list item, list items for everyone!</li>
					</ul>
                </td>
            </tr>
            <tr>
                <td style="padding: 0 20px 20px;">
                    <!-- Button : BEGIN -->
                    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                        <tr>
                            <td class="button-td button-td-primary" style="border-radius: 4px; background: #222222;">
							     <a class="button-a button-a-primary" href="https://google.com/" style="background: #222222; border: 1px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; display: block; border-radius: 4px;"><span class="button-link" style="color:#ffffff">Centered Primary Button</span></a>
							</td>
                        </tr>
                    </table>
                    <!-- Button : END -->
                </td>
            </tr>

        </table>
    </td>
</tr>
<!-- 1 Column Text + Button : END -->

<!-- Background Image with Text : BEGIN -->
<tr>
    <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
    <td valign="middle" style="text-align: center; background-image: url('http://placehold.it/600x230/222222/666666'); background-color: #222222; background-position: center center !important; background-size: cover !important;">
        <!--[if gte mso 9]>
        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:175px; background-position: center center !important;">
        <v:fill type="tile" src="http://placehold.it/600x230/222222/666666" color="#222222" />
        <v:textbox inset="0,0,0,0">
        <![endif]-->
        <div>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="middle" style="text-align: center; padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #ffffff;">
                        <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent laoreet malesuada cursus. Maecenas scelerisque congue eros eu posuere. Praesent in felis ut velit pretium lobortis rhoncus ut&nbsp;erat.</p>
                    </td>
                </tr>
            </table>
        </div>
        <!--[if gte mso 9]>
        </v:textbox>
        </v:rect>
        <![endif]-->
    </td>
</tr>
<!-- Background Image with Text : END -->

<!-- 2 Even Columns : BEGIN -->
<tr>
    <td valign="top" style="padding: 10px; background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <!-- Column : BEGIN -->
                <td class="stack-column-center">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <img src="http://placehold.it/270" width="270" height="270" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td class="stack-column-center">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <img src="http://placehold.it/270" width="270" height="270" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
            </tr>
        </table>
    </td>
</tr>
<!-- 2 Even Columns : END -->

<!-- 3 Even Columns : BEGIN -->
<tr>
    <td valign="top" style="padding: 10px; background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <!-- Column : BEGIN -->
                <td width="33.33%" class="stack-column-center">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="33.33%" class="stack-column-center">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="33.33%" class="stack-column-center">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
            </tr>
        </table>
    </td>
</tr>
<!-- 3 Even Columns : END -->

<!-- Thumbnail Left, Text Right : BEGIN -->
<tr>
    <td dir="ltr" valign="top" width="100%" style="padding: 10px; background-color: #ffffff;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <!-- Column : BEGIN -->
                <td width="33.33%" class="stack-column-center">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td dir="ltr" valign="top" style="padding: 0 10px;">
                                <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="66.66%" class="stack-column-center">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px; text-align: left;" class="center-on-narrow">
                                <h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 22px; color: #333333; font-weight: bold;">Class aptent taciti sociosqu</h2>
                                <p style="margin: 0 0 10px 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                                <!-- Button : BEGIN -->
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
                                    <tr>
                                        <td class="button-td button-td-primary" style="border-radius: 4px; background: #222222;">
										     <a class="button-a button-a-primary" href="https://google.com/" style="background: #222222; border: 1px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; display: block; border-radius: 4px;"><span class="button-link" style="color:#ffffff">Primary Button</span></a>
										</td>
                                  </tr>
                              </table>
                              <!-- Button : END -->
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
            </tr>
        </table>
    </td>
</tr>
<!-- Thumbnail Left, Text Right : END -->

<!-- Thumbnail Right, Text Left : BEGIN -->
<tr>
    <td dir="rtl" valign="top" width="100%" style="padding: 10px; background-color: #ffffff;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <!-- Column : BEGIN -->
                <td width="33.33%" class="stack-column-center">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td dir="ltr" valign="top" style="padding: 0 10px;">
                                <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="66.66%" class="stack-column-center">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px; text-align: left;" class="center-on-narrow">
                                <h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 22px; color: #333333; font-weight: bold;">Class aptent taciti sociosqu</h2>
                                <p style="margin: 0 0 10px 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                                <!-- Button : BEGIN -->
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
                                    <tr>
                                        <td class="button-td button-td-primary" style="border-radius: 4px; background: #222222;">
										     <a class="button-a button-a-primary" href="https://google.com/" style="background: #222222; border: 1px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; display: block; border-radius: 4px;"><span class="button-link" style="color:#ffffff">Primary Button</span></a>
										</td>
                                    </tr>
                                </table>
                                <!-- Button : END -->
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- Column : END -->
            </tr>
        </table>
    </td>
</tr>
<!-- Thumbnail Right, Text Left : END -->

<!-- Clear Spacer : BEGIN -->
<tr>
    <td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px;">
        &nbsp;
    </td>
</tr>
<!-- Clear Spacer : END -->

<!-- 1 Column Text : BEGIN -->
<tr>
    <td style="background-color: #ffffff;">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent laoreet malesuada cursus. Maecenas scelerisque congue eros eu posuere. Praesent in felis ut velit pretium lobortis rhoncus ut&nbsp;erat.
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 1 Column Text : END -->
@stop