@extends('admin.inc.template')

@section ('content')
<div class="container">
	<h2>Update Password</h2>
	<div class="alert alert-warning">
		{!! $msg !!} Please update your password right now.
	</div> 

	<div class="panel">
		<div class="panel-body">

			<form action="" method="post">
				{{ csrf_field() }}
				@if($errors->any())
					@foreach($errors->messages() as $err)
					<div class="alert alert-danger">{!! $err[0] !!}</div>
					@endforeach
				@endif

				<div class="form-group">
					<label>Email</label>
					<div>{{ Auth::user()->email }}</div>
				</div>
				<br>
				<br>
				<div class="form-group">
					<label>New Password</label>
					<input type="password" name="new_password" class="form-control">
				</div>
				<div class="form-group">
					<label>New Password Confirmation</label>
					<input type="password" name="new_password_confirmation" class="form-control">
				</div>

				<button type="submit" class="btn btn-primary">Update Password</button>

			</form>

			
		</div>
	</div>



</div>
@stop