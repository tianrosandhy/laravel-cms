<div class="card">
	<div class="card-block">
		<table class="table data data-table dataTable">
			<thead>
				<tr>
					@foreach($columns as $key=>$val)
						<th>{{ $key }}</th>
					@endforeach
				</tr>
			</thead>
			<thead class="search">
				<tr>
					@foreach($columns as $key=>$val)
						<th {!! isset($val['custom']) ? 'data-custom field="'.$val['col'].'"' : '' !!}>{{ $val['search'] ? $val['col'] : "" }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

@push ('scripts')
<script>
var tb_data;

$(function(){
//tweak sompret mengatasi bug HTTP Not Found, padahal ga ada masalah URL	
	setTimeout(function(){
		//initialize datatable
		tb_data = $("table.data-table").DataTable({
			'processing': true,
			'serverSide': true,
			'autoWidth' : false,
			'searching'	: false,
			'filter'	: false,
			'ajax'		: {
				type : 'POST',
				url	: ADMIN_URL + '{{ isset($config['server']) ? $config['server'] : '' }}',
				dataType : 'json',
				data : function(data){
					{!! $search !!}
				}
			},
			"drawCallback": function(settings) {
				$('[data-init="switchery"]').each(function() {
					var el = $(this);
					new Switchery(el.get(0), {
						size : el.data("size")
					});
				});
			},
			@if(isset($config['hideOrder']))
			'columnDefs' : [
				@foreach($config['hideOrder'] as $colnum)
				{
					'targets' : {{ $colnum }},
					'orderable' : false
				},
				@endforeach
			],
			@endif
			@if(isset($config['sort']))
			"aaSorting": [{!! json_encode($config['sort']) !!}],
			@endif
		});

		//after render, jika ada field searchable custom langsung dimunculin juga
		@if(count($custom) > 0)
			@foreach($custom as $key => $item)

			$("input.search#"+'{{ $key }}').replaceWith('{!! $item !!}');

			@endforeach
		@endif

	}, 500);
});
</script>
@endpush