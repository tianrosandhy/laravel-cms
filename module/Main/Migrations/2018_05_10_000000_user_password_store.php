<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPasswordStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cms_password_store')) {
            Schema::create('cms_password_store', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('password_hash');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cms_password_store')) {
            Schema::dropIfExists('cms_password_store');
        }
    }
}
