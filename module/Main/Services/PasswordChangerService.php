<?php
namespace Module\Main\Services;

use Illuminate\Contracts\Foundation\Application;
use Module\Main\Http\Models\UserPasswordStore;
use Auth;

class PasswordChangerService
{
    public $regex = '/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,32}$/';

	public function __construct(Application $app){
		$this->app = $app;
		$this->model = new UserPasswordStore();
	}


	public function passwordUpdated(){
		$n = $this->model->resetTime;
        if(Auth::check()){
            $data = Auth::user();
            $maxOldPassword = date('Y-m-d H:i:s', strtotime('-'.$n.' month'));
            $cek = UserPasswordStore::where('created_at', '>', $maxOldPassword)->get();
            if(count($cek) > 0){
                return false;
            }

            //wajib ganti password jika terdeteksi
            return self::forceChangePassword();
        }
        return false;
    }

    protected function forceChangePassword(){
        $target = admin_url('update-password', true);
        return '<script>
            window.location = "'.$target.'";
        </script>';
    }


    public function getUserData(){
        $current = Auth::user();
        $data = UserPasswordStore::where('user_id', $current->id)->orderBy('created_at', 'DESC')->first();
        return $data;
    }

}