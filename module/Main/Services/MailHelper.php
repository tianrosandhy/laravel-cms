<?php

namespace Module\Main\Services;

use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;


class MailHelper
{
    //
	public $email;
	public $name;
	public $host;
	public $message;

	public $reformat = ['gmail.com'];



	public function __construct(Application $app){
		$this->app = $app;
	}

	/*
	- Cara pakai : 
	- Masukkan email di facade static ::check()
	*/


    public function check($email=''){
        if(strlen($email) > 0){
            self::set($email);
        }

        if(count($this->message) > 0)
            return $this->message;
        else{
            return [
                'error' => false,
                'email' => $this->email,
            ];
        }
    }

    public function set($eml){
        //preparing email validation lists

    	$this->email = $eml;

    	if(self::check_format()){
    		$exp = explode("@", $this->email);
	    	$this->name = $exp[0];
	    	$this->host = $exp[1];
    	}
    	else{
    		$this->message['error'] = 'Invalid email format.';
    	}

    	if(!self::check_dns()){
    		$this->message['error'] = 'Mail DNS provider not found';
    	}

    	if(self::check_disposable()){
    		$this->message['error'] = 'The email that you entered is listed in blacklist';
    	}

    	self::reformat_email();
    }
















    protected function check_format(){
    	if(filter_var($this->email, FILTER_VALIDATE_EMAIL)){
    		return true;
    	}
    	return false;
    }

    protected function reformat_email(){
    	//mengabaikan tanda titik dan seluruh alphabet setelah tanda +
    	//hanya untuk host yang ditandai untuk diformat ulang
    	if(in_array($this->host, $this->reformat)){
    		$this->name = str_replace(".", "", $this->name);

    		$n = strpos($this->name, "+");
    		if($n){
    			$this->name = substr($this->name, 0, $n);
    		}
    	}
    	$this->email = $this->name."@".$this->host;
    }


    protected function check_dns(){
    	if(strlen($this->host) > 0){
	    	if(checkdnsrr($this->host, "MX")){
	    		return true;
	    	}
	    	return false;
    	}
    	else
    		return false;
    }

    protected function check_disposable(){
    	$data = config('blacklist.mailhost');  
    	$list = preg_split("/[\r\n]+/", $data);

    	if(in_array($this->host, $list)){
    		return true;
    	}
    	return false;

    }



}
