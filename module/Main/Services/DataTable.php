<?php
namespace Module\Main\Services;
use Illuminate\Contracts\Foundation\Application;
use DB;


class DataTable {
	protected $configName = 'DataTable';

	public 
		$fillable = [
			'columns',
			'search',
			'model',
			'data'
		],

		$columns,
		$search,
		$model,
		$data;

	public function __construct(Application $app){
		$this->app = $app;
	}





	//GLOBAL SETTER AND GETTER
	public function set($property, $value){
		if(self::checkProperty($property)){
			$this->{$property} = $value;
		}
	}

	public function get($property){
		if(self::checkProperty($property)){
			return $this->{$property};
		}
	}

	protected function checkProperty($prop){
		if(in_array($prop, $this->fillable))
			return true;
		return false;
	}








	//pengolahan
	public function generate($withstat = true, $custom_where = [], $custom_order = []){

		$where_data = self::where_building();
		if($custom_where){
			$where_data = array_merge($where_data, [$custom_where]);
		}

		$result = self::query_result($withstat, $where_data);
		$out['draw'] = $this->data['draw'];

		$out['recordsTotal'] = count($result);
		$out['recordsFiltered'] = count($result);


		$filtered = self::query_result($withstat, $where_data, true, $custom_order);
		$out['result'] = $filtered;
		$out['data'] = self::blank_action();

		return $out;
	}

	protected function blank_action(){
		$rt = [];
		for($i=0; $i<count($this->data['columns']); $i++){
			array_push($rt, '');
		}
		$ret[] = $rt;
		return $ret;
	}

	protected function where_building(){
		$where_data = []; 
		foreach($this->columns as $col){
			if(isset($col['search'])){
				if($col['search'] == true){
					if(isset($this->data[$col['col']])){
						$where_data[] = [$col['col'], '%'.$this->data[$col['col']].'%'];
					}
				}
			}
		}
		return $where_data;
	}

	protected function query_result($withstat=true, $where_data = [], $limit=false, $custom_order = []){
		$result = $this->model;

		//if there is control stat column
		if($withstat){
			$result = $result->where('stat', '<>', 9);
		}

		//filter by search query
		if(count($where_data) > 0){
			foreach($where_data	as $where){
				if(count($where) == 2){
					$result = $result->where($where[0], 'like' , $where[1]);
				}
				else if(count($where) == 3){
					$result = $result->where($where[0], $where[1], $where[2]);
				}
			}
		}

		//get orderby
		$index = $this->data['order'][0]['column'];
		$arah = $this->data['order'][0]['dir'];

		//just limit for query result
		if($limit){
			$result = $result->skip($this->data['start'])
				->take($this->data['length']);
		}

		$kolom = array_values($this->columns);

		if(isset($kolom[$index])){
			$result = $result->orderBy($kolom[$index]['col'], $arah);
		}
		if(count($custom_order) > 0){
			$result = $result->orderBy($custom_order[0], $custom_order[1]);
		}

		return $result->get();
	}
















	//static helper method
	public function get_from_tb($tbname, $id, $get, $field='id'){
		$cek = DB::table($tbname)
		->where('stat','<>',9)
		->where($field, $id)
		->first();

		if(count($cek) > 0){
			$row = (array)$cek;
			return $row[$get];
		}
		else
			return null;
	}

    public function generate_search($data){
    	$out = [];
		foreach($data as $dt){
			if($dt['search'])
				$out[] = "data.".$dt['col']." = $('#". $dt['col'] ."').val()";
		}
		return implode(", ", $out);
    }

    public function customComboBox($name, $tb, $label, $pk='id', $where=[], $search=true){
    	$data = DB::table($tb)
    		->where('stat', 1);

    	if(count($where) > 0){
    		$data = $data->where($where);
    	}

    	$data = $data->get()->pluck($label, $pk)->toArray();
    	return self::manualComboBox($name, $data, $search);
    }

    public function manualComboBox($name, $data=[], $search=true){
    	if($search){
	    	$html = '<select name="'.$name.'" class="form-control search" id="'.$name.'">';
	    	$html .= '<option value="">Search '.ucfirst($name).'</option>';
    	}
    	else{
	    	$html = '<select name="'.$name.'" class="form-control">';
	    	$html .= '<option value=""></option>';
    	}
    	foreach($data as $id => $val){
    		$html .= '<option value="'.$id.'">'.$val.'</option>';
    	}
    	$html .= '</select>';

    	return $html;
    }	













    //View Generate
    public function tableGenerate($config=[]){
    	$config['server'] = isset($config['server']) ? $config['server'] : '/';




    	//generate custom searchbox
    	$custom = [];
    	if(isset($config['custom'])){
    		foreach($config['custom'] as $arr){
    			$custom[$arr[0]] = self::customComboBox($arr[0], $arr[1], $arr[2], $arr[3]);
    		}
    	}

    	if(isset($config['customManual'])){
    		foreach($config['customManual'] as $arr){
    			$custom[$arr[0]] = self::manualComboBox($arr[0], $arr[1]);
    	}
    		}

	   	return view('main::table-general', [
    		'columns' => $this->columns,
    		'config' => $config,
    		'search' => self::generate_search($this->columns),
    		'custom' => $custom
    	]);
    }




}