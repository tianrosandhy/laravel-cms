<?php
namespace Module\Main\Services;
use Illuminate\Contracts\Foundation\Application;

class Cache {
	protected $configName = 'Cache';

	public function __construct(Application $app){
		$this->app = $app;
	}

	public function merge($data, $type='css'){
		$out = '';
		foreach($data as $file_loc){
			$out .= register_single($file_loc, $type);
		}

		return $out;
	}
}