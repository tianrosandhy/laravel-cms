<?php
namespace Module\Main\Support;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class MailHelperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mailhelper-facade';
    }
}
