<?php
namespace Module\Main\Support;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class PasswordChangerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'password-changer-facade';
    }
}
