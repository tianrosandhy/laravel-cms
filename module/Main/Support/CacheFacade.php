<?php
namespace Module\Main\Support;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class CacheFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cache-facade';
    }
}
