<?php
namespace Module\Main\Support;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class DataTableFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'datatable-facade';
    }
}
