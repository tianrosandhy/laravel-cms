<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->get('/', 'DashboardController@index')->name('admin.dashboard');
	$route->get('login', 'DashboardController@login')->name('admin.login');
	$route->get('logout', 'DashboardController@logout')->name('admin.logout');

    $route->get('update-password', 'DashboardController@updatePassword');
    $route->post('update-password', 'DashboardController@postUpdatePassword');


    $route->post('login', 'Auth\LoginController@login');
    $route->post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    $route->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $route->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $route->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $route->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $route->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $route->post('password/reset', 'Auth\ResetPasswordController@reset');

	//utk upload gallery
	$route->post('api/gallery', 'Api\\GalleryApi@index');
	$route->post('api/gallery/ckeditor', 'Api\\GalleryApi@ckeditor');
});

Route::post('api/cropper', 'Api\\Cropper@index');
Route::post('api/cropper/remove', 'Api\\Cropper@remove');