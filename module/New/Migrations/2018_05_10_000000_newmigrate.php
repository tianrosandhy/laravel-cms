<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Newmigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cms_new')) {
            Schema::create('cms_new', function (Blueprint $table) {
                $table->increments('id');
                
                $table->timestamps();
                $table->tinyinteger('stat');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cms_new')) {
            Schema::dropIfExists('cms_new');
        }
    }
}
