<?php
namespace Module\New\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DataTableModel;



class NewController extends Controller
{
	public 	$request,
			$columns,
			$model;

	public function __construct(Request $req){
		$this->request = $req;
	}

	public function index(){
		
	}

	public function create(){

	}

	public function store(){

	}

	public function edit($id=0){

	}

	public function update($id=0){

	}

	public function destroy($id=0){

    }
}