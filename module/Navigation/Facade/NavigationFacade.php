<?php 

namespace Module\Navigation\Facade;

use Illuminate\Support\Facades\Facade;

class NavigationFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Navigation\\Controllers\\NavigationController';
    }
}