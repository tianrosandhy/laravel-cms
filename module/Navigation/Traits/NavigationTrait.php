<?php
namespace Module\Navigation\Traits;

use Form;

use Request;
use Module\Navigation\Models\NavigationModel;
use Module\Navigation\Models\NavigationGroupModel;

use Module\Main\Http\Controllers\Api\DatatableApi;

trait NavigationTrait
{

    public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$model = new NavigationGroupModel();
			$data = $model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}

	//method utk FE, memanggil navigasi berdasarkan nama
	static function getNavigation($group_name){
		$all_data = self::grouping();
		$get = NavigationGroupModel::where('title', $group_name)
			->where('stat', '<>', 9)
			->first();
		if(count($get) > 0){
			if(isset($all_data[$get->id]))
				return $all_data[$get->id];
		}
		return [];
	}

	static function grouping(){
		$sql = NavigationModel::where('stat', 1)
            ->orderBy('parent', 'ASC')
            ->orderBy('sort_no', 'ASC')
            ->get();

        $out = [];
        $lev3 = [];
        foreach($sql as $row){
        	if($row->parent == 0){
	        	$out[$row->group_id][$row->id] = [
	        		'id' => $row->id,
	        		'title' => $row->title,
	        		'url' => $row->url
	        	];
        	}
        	else{
        		if(isset($out[$row->group_id][$row->parent])){
        			$out[$row->group_id][$row->parent]['children'][$row->id] = [
		        		'id' => $row->id,
		        		'title' => $row->title,
		        		'url' => $row->url
        			];
        		}
        		else{
        			//kemungkinan ini struktur level 3
        			$lev3[$row->group_id][$row->parent][$row->id] = [
		        		'id' => $row->id,
		        		'title' => $row->title,
		        		'url' => $row->url
        			];
        		}
        	}
        }


        //jika ada array level 3, masukkan ke array utama
        foreach($out as $grp_id => $hehe){
	        foreach($hehe as $index=>$data){
	        	if(isset($data['children'])){
	        		foreach($data['children'] as $childid => $child){
	        			if(isset($lev3[$grp_id])){
		        			if(array_key_exists($childid, $lev3[$grp_id])){
		        				$out[$grp_id][$index]['children'][$childid]['children'] = $lev3[$grp_id][$childid];
		        			}
	        			}
	        		}
	        	}
	        }
        }

        return $out;
	}


	static function list_group($include_blank = true){
		$sql = NavigationGroupModel::where('stat', 1)->get()->pluck('title', 'id')->toArray();
		if($include_blank){
			$sql = [0 => ''] + $sql;
		}
		return $sql;
	}

	public function list_navigation($group=0){
		$sql = NavigationModel::where('stat', 1);
		if($group > 0){
			$sql = $sql->where('group_id', $group);
		}
		$sql = $sql->where('parent',0);
		$sql = $sql->orderBy('group_id');
		$sql = $sql->orderBy('sort_no');
		$sql = $sql->get()->pluck('title', 'id')->toArray();

		$sql = [0 => ''] + $sql;

		return response()->json($sql);
	}

	static function get_last_order($group=0, $parent=0){
		$sql = NavigationModel::where('stat', 1);
		if($group > 0){
			$sql = $sql->where('group_id', $group);
		}
		if($parent > 0){
			$sql = $sql->where('parent', $parent);
		}
		$sql->orderBy('group_id');
		$sql->orderBy('sort_no', 'DESC');
		$sql = $sql->first();

		if(count($sql) > 0){
			return intval($sql->sort_no) + 1;
		}
		else
			return 1;
	}
}