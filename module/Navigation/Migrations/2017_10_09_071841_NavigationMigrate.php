<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NavigationMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_navigation', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->integer('parent');
            $tb->string('url',190);
            $tb->integer('group_id');
            $tb->integer('sort_no');
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_navigation');
    }
}
