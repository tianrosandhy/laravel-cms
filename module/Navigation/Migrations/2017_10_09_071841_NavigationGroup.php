<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NavigationGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_navigation_group', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->text('description')->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_navigation_group');
    }
}
