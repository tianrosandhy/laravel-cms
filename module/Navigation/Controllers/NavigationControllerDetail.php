<?php

namespace Module\Navigation\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\GalleryModel;

use Module\Navigation\Models\NavigationModel;
use Module\Navigation\Models\NavigationGroupModel;
use Module\Navigation\Traits\NavigationTrait;


class NavigationControllerDetail extends Controller
{
	public 	$request, 
			$model,
			$columns;
	
	use NavigationTrait;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new NavigationModel();
		$this->group = new NavigationGroupModel();
	}

    public function index(){
    	return view('navigation::detail')->with([
            'group' => $this->group->where('stat',1)->get(),
            'sort_data' => self::grouping(),
    	]);
    }

    public function store(){
        $post = $this->request->all();
        $validate = Validator::make($post, [
            'title' => 'required',
            'url' => 'required',
            'group' => 'required'
        ]);

        if($validate->fails()){
            $out['type'] = 'error';
            foreach ($validate->messages()->getMessages() as $field_name => $messages)
            {
                $out['message'] = $messages[0];
                continue;
            }
            return response()->json($out);
        }
        else{
            $sort_no = self::get_last_order($post['group']);
            $save = NavigationModel::insert([
                'title' => $post['title'],
                'parent' => intval($post['parent']),
                'url' => $post['url'],
                'group_id' => $post['group'],
                'sort_no' => $sort_no,
                'stat' => 1
            ]);
            $out['type'] = 'success';
            $out['message'] = 'Navigation inserted successfully';
            return response()->json($out);
        }
    }

    public function edit($id){
        //get data id yang ingin diedit
        $sql = NavigationModel::find($id);
        if(count($sql) == 0){
            $out = ['type' => 'error', 'message' => 'Navigation not found'];
        }
        else{
            $data = json_decode(json_encode($sql), true);
            $out = ['type' => 'success', 'message' => $data];
        }
        return response()->json($out);
    }

    public function update($id){
        $sql = NavigationModel::find($id);
        if(count($sql) == 0){
            $out = ['type' => 'error', 'message' => 'Navigation item not found'];
        }
        else{
            $post = $this->request->all();
            $validate = Validator::make($post, [
                'title' => 'required',
                'url' => 'required'
            ]);

            if($validate->fails()){
                $out['type'] = 'error';
                foreach ($validate->messages()->getMessages() as $field_name => $messages)
                {
                    $out['message'] = $messages[0];
                    continue;
                }
                return response()->json($out);
            }
            else{
                //tidak ada perubahan order
                $sql->title = $post['title'];
                $sql->parent = $post['parent'];
                $sql->url = $post['url'];
                $sql->stat = 1;
                $sql->save();

                $out = ['type' => 'success', 'message' => 'Navigation has been updated'];
            }
        }
        return response()->json($out);
    }


    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
                    $this->model->where('parent', $id)->update(['stat'=> 9]);
                    
                    $out['type'] = 'success';
		    		$out['message'] = "Berhasil menghapus data";
	    		}
	    		else{
                    $out['type'] = 'error';
	    			$out['message'] = "Data not found";
	    		}
	    	}
	    	else{
                $out['type'] = 'error';
	    		$out['message'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }
    

    public function reorder(){
        $post = $this->request->all();
        if(!isset($post['json']) || !isset($post['group'])){
            $out['type'] = 'error';
            $out['message'] = 'Invalid order data specified';
        }
        else{
            $data = json_decode($post['json'], true);

            //hapus data order seluruh yang ada di group tersebut
            $upd = NavigationModel::where('group_id', $post['group'])->update([
                'sort_no' => 0,
                'parent' => 0
            ]);


            //update sort logic
            foreach($data as $level1){
                $id = $level1['id'];
                $last_order = self::get_last_order($post['group']);
                //update first parent with order
                NavigationModel::where('id', $id)->update(['sort_no' => $last_order]);

                if(isset($level1['children'])){
                    foreach($level1['children'] as $level2){
                        $id2 = $level2['id'];
                        $last_order = self::get_last_order($post['group'], $level1['id']);
                        NavigationModel::where('id', $id2)->update(['sort_no' => $last_order, 'parent' => $level1['id']]);

                        if(isset($level2['children'])){
                            foreach($level2['children'] as $level3){
                                $id3 = $level3['id'];
                                $last_order = self::get_last_order($post['group'], $level2['id']);
                                NavigationModel::where('id', $id3)->update(['sort_no' => $last_order, 'parent' => $level2['id']]);
                            }
                        }

                    }
                }
            }

            //kalau loop sudah selesai dieksekusi, artinya, ordering sudah selesai.
            $out['type'] = 'success';
            $out['message'] = 'Navigation ordered successfully';
        }

        return response()->json($out);

    }
}
