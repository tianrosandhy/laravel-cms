<?php

namespace Module\Navigation\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\GalleryModel;

use Module\Navigation\Models\NavigationGroupModel;
use Module\Navigation\Traits\NavigationTrait;
use Module\Navigation\Traits\NavigationDatatable;
use DataTable;

class NavigationController extends Controller
{
	public 	$request, 
			$model;
	
	use NavigationTrait;
    use NavigationDatatable;

    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new NavigationGroupModel();

        DataTable::set('columns', $this->columns);
        DataTable::set('model', $this->model);
        DataTable::set('data', $this->request->all());
	}

    public function index(){
    	return view('navigation::index')->with([
            'title' => 'Navigation Group',
            'table' => DataTable::tableGenerate([
                'server' => '/navigation/table',
                'hideOrder' => [2,3]
            ])
    	]);
    }

    public function store(){
    	$rule = [
    		'title' => 'required|max:100'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

    	$cek = $this->model
    		->where('title', $this->request->title)
    		->where('stat', '<>', 9)
    		->get();

    	if(count($cek) > 0){
            return ajax_response('Data navigasi dengan nama tersebut sudah ada');
    	}
    	else{
    		$this->model->insert([
    			'title' => $this->request->title,
    			'description' => $this->request->description,
    			'stat' => 1
    		]);
            return ajax_response('Berhasil menyimpan navigasi', 'success');
    	}

    	return response()->json($out);
    }

    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$model = new NavigationGroupModel();
	    		$cek = $model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:100'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
             return ajax_response($validate->messages()->first());
    	}

    	$cek = $this->model
    		->where('title', $this->request->title)
    		->where('stat', '<>', 9)
    		->where('id', '<>', $id)
    		->get();

    	if(count($cek) > 0){
            return ajax_response('Data navigasi dengan nama tersebut sudah ada');
    	}
    	else{

    		$this->model->where('id', $id)->update([
    			'title' => $this->request->title,
    			'description' => $this->request->description,
    		]);
            return ajax_response('Berhasil mengupdate data navigasi', 'success');
    	}

    }
    

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->group->find($id);
	    		if($cek){
		    		$cek->update(['stat' => 9]);
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }
    
}
