<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('navigation', 'NavigationController@index')->name('admin.navigation.index');
	Route::post('navigation', 'NavigationController@store')->name('admin.navigation.store');
	Route::post('navigation/table', 'NavigationController@getPost');
	Route::post('navigation/switch', 'NavigationController@getSwitch');
	Route::post('navigation/edit', 'NavigationController@edit')->name('admin.navigation.edit');
	Route::post('navigation/update/{id?}', 'NavigationController@update')->name('admin.navigation.update');
	Route::post('navigation/destroy', 'NavigationController@destroy')->name('admin.navigation.destroy');


	Route::get('navigation/detail', 'NavigationControllerDetail@index')->name('admin.nav.index');
	Route::post('navigation/detail', 'NavigationControllerDetail@store')->name('admin.nav.store');
	Route::post('navigation/detail/table', 'NavigationControllerDetail@getPost');
	Route::post('navigation/detail/switch', 'NavigationControllerDetail@getSwitch');
	Route::post('navigation/detail/edit/{id?}', 'NavigationControllerDetail@edit')->name('admin.nav.edit');
	Route::post('navigation/detail/update/{id?}', 'NavigationControllerDetail@update')->name('admin.nav.update');
	Route::post('navigation/detail/destroy/{id?}', 'NavigationControllerDetail@destroy')->name('admin.nav.destroy');
	Route::post('navigation/list-navigation/{group}', 'NavigationControllerDetail@list_navigation');
	Route::post('navigation/detail/reorder', 'NavigationControllerDetail@reorder');
});
