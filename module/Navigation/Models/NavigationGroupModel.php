<?php

namespace Module\Navigation\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class NavigationGroupModel extends Model
{
    //
    protected $table = "cms_navigation_group";
    protected $fillable = [
    	'id',
    	'title',
    	'description',
    	'created_at',
    	'updated_at',
    	'stat'
    ];

    public function content(){
        return $this->hasMany('Module\\Navigation\\Models\\NavigationModel', 'group_id', 'id');
    }
}
