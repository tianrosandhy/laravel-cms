<?php

namespace Module\Navigation\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class NavigationModel extends Model
{
    //
    protected $table = "cms_navigation";
    protected $fillable = [
    	'id',
    	'title',
    	'parent',
    	'url',
    	'group_id',
    	'sort_no',
    	'created_at',
    	'updated_at',
    	'stat', 
    ];

    public function group(){
        return $this->belongsTo('Module\\Navigation\\Models\\NavigationGroupModel', 'group_id', 'id');
    }

}
