<?php
namespace Module\Navigation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\Router;

class ModuleNavigationProvider extends ServiceProvider
{
	protected $namespace = 'Module\Navigation\Controllers';

	public function boot(){
		$this->loadMigrationsFrom(realpath(__DIR__."/Migrations"));
	}

	protected function mapping(Router $router){
		$router->group(['namespace' => $this->namespace, 'middleware' => 'web'], function($router){
			require realpath(__DIR__."/routes.php");
		});
	}

	public function register(){
		$this->mapping($this->app->router);
		$this->loadViewsFrom(realpath(__DIR__."/Views"), 'navigation');
	}
}