@extends('admin.inc.template')
@section('content')

	<h2>Navigation Item</h2>

	@foreach($group as $row)
		<div class="card" data-group="{{ $row->id }}">
			<div class="card-header separator">
				<div class="card-title">{{ $row->title }}</div>
				<div class="pull-right">
					@if(hasAccess('admin.nav.store'))
					<a class="btn btn-sm btn-primary btn-add-group" data-group="{{ $row->id }}">
						<i class="fa fa-plus"></i>
					</a>
					@endif
				</div>
			</div>
			<div class="card-block">
				<div class="dd" data-group="{{ $row->id }}">
				    <ol class="dd-list">
				    	@if(isset($sort_data[$row->id]))
								@foreach($sort_data[$row->id] as $index=>$level1)
					        <li class="dd-item dd3-item" data-id="{{ $index }}">
					            <div class="dd-handle dd3-handle">
					            </div>
					            <div class="dd3-content">
					            	{{ $level1['title'] }} -<em> {{ $level1['url'] }}</em>
					            	<div class="btn-group pull-right">
						            	<a class="label label-info edit-navigation" data-id="{{ $level1['id'] }}">
						            		<i class="fa fa-pencil"></i>
						            	</a>
						            	<a class="label label-danger delete-button" data-id="{{ $level1['id'] }}">
						            		<i class="fa fa-trash"></i>
						            	</a>
					            	</div>
					            </div>

					            @if(isset($level1['children']))
				            		<ol class="dd-list">
					            	@foreach($level1['children'] as $ind => $level2)
					            		<li class="dd-item dd3-item" data-id="{{ $ind }}">
					            			<div class="dd-handle dd3-handle">
					            			</div>
					            			<div class="dd3-content">
					            				{{ $level2['title'] }} -<em> {{ $level2['url'] }}</em>
								            	<div class="btn-group pull-right">
									            	<a class="label label-info edit-navigation" data-id="{{ $level2['id'] }}">
									            		<i class="fa fa-pencil"></i>
									            	</a>
									            	<a class="label label-danger delete-button" data-id="{{ $level2['id'] }}">
									            		<i class="fa fa-trash"></i>
									            	</a>
								            	</div>
					            			</div>

					            			@if(isset($level2['children']))
							            		<ol class="dd-list">
								            	@foreach($level2['children'] as $idd => $level3)
								            		<li class="dd-item dd3-item" data-id="{{ $idd }}">
								            			<div class="dd-handle dd3-handle">
								            			</div>
								            			<div class="dd3-content">
								            				{{ $level3['title'] }} -<em> {{ $level3['url'] }}</em>
											            	<div class="btn-group pull-right">
												            	<a class="label label-info edit-navigation" data-id="{{ $level3['id'] }}">
												            		<i class="fa fa-pencil"></i>
												            	</a>
												            	<a class="label label-danger delete-button" data-id="{{ $level3['id'] }}">
												            		<i class="fa fa-trash"></i>
												            	</a>
											            	</div>
								            			</div>
								            		</li>
								            	@endforeach
							            		</ol>
					            			@endif

					            		</li>
					            	@endforeach
				            		</ol>
					            @endif

					        </li>
				        @endforeach
				       @endif
				    </ol>
				</div>	

				<a href="" class="btn btn-primary reorder" data-group="{{ $row->id }}" style="display:none">Reorder</a>

			</div>
		</div>
	@endforeach

	<div class="output" style="display:none;"></div>

@endsection


@section ('modal')
	<div class="modal fade slide-up" id="addNavigation" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
						<h4 class="title">Add Navigation</h4>
					</div>
					<div class="modal-body">
						<form action="" method="post" class="form-ajax">
							{{ csrf_field() }}
							<div class="form-group form-group-default">
								<label>Title</label>
								<input type="text" name="title" class="form-control">
							</div>
							<div class="form-group form-group-default">
								<label>URL</label>
								<input type="text" name="url" class="form-control">
							</div>
							<div class="form-group form-group-default">
								<label>Menu Group</label>
								<select name="group" class="form-control">
									@foreach(Navigation::list_group() as $indx => $grp)
									<option value="{{ $indx }}">{{ $grp }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group form-group-default">
								<label>Parent</label>
								<select name="parent" class="form-control">
								</select>
							</div>

							<div>
								<button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection



@push ('script')
<script>
	$(function(){
		//multiple initialize by group id
		@foreach($sort_data as $index => $data)
			//initialize nestable
			$(".dd[data-group="+{{ $index }}+"]").nestable({
				maxDepth : 3
			});

			//initialize value holder
			$(".output").append('<div data-content="{{ $index }}"></div>');

			serializeContext({{ $index }});
			$(".dd[data-group="+ {{ $index }} +"]").on('change', function(){
				showReorderButton({{ $index}});
			});
		@endforeach
	});
</script>
<script>
	function showReorderButton(index){
		$(".reorder[data-group="+index+"]").slideDown();
	}

	$(".reorder").on('click', function(e){
		e.preventDefault();
		group = $(this).data('group');
		serializeContext(group, true);
	});


	function serializeContext($index, send=false){
		val = $(".dd[data-group="+ $index +"]").nestable('serialize');
		json = window.JSON.stringify(val);
		$(".output [data-content="+ $index +"]").html(json);
		
		if(send == true){
			//send reorder data via ajax
			$.ajax({
				url : ADMIN_URL + '/navigation/detail/reorder',
				dataType : 'json',
				type : 'POST',
				data : {
					'group' : $index,
					'json' : json
				}
			}).done(function(resp){
				sweet_alert(resp.type, resp.message);
				$(".reorder").slideUp();
			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});
		}

	}




	$(".btn-add-group").on('click', function(){
		var group = $(this).data('group');
		add_action(group);
	});

	function add_action(group){
		var dataid = group;
		var form_add_action = ADMIN_URL + '/navigation/detail';

		//load parent ajax value
		$.ajax({
			url : ADMIN_URL + '/navigation/list-navigation/'+dataid,
			type : 'POST',
			dataType : 'json'
		}).done(function(resp){
			options = '';
			$.each(resp, function(key, val){
				options += '<option value="'+key+'">'+val+'</option>';
			});
			$("#addNavigation form").attr('action', form_add_action);
			$("#addNavigation form [name=parent]").html(options);
			$("#addNavigation h4.title").html('Add Navigation');
			$("#addNavigation [name=group]").removeAttr('disabled');

			//load modal
			$("#addNavigation").modal('show');
			$("#addNavigation form [name=group]").val(dataid);
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});

	}



	$("#addNavigation form").on('submit', function(e){
		e.preventDefault();
		trget = $(this).attr('action');
		val = $(this).serialize();
		$.ajax({
			url : trget,
			type : 'POST',
			dataType : 'json',
			data : val
		}).done(function(resp){
			sweet_alert(resp.type, resp.message);
			if(resp.type == 'success'){
				$("#addNavigation").modal('hide');
				location.reload();
			}
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	});


	$(".edit-navigation").on('click', function(){
		var id = $(this).data('id');
		var group = $(this).closest('.dd[data-group]').data('group');
		var form_target = ADMIN_URL + '/navigation/detail/update/'+id;

		//get data via ajax
		$.ajax({
			url : ADMIN_URL + '/navigation/detail/edit/'+id,
			dataType : 'json',
			type : 'POST'
		}).done(function(resp){
			if(resp.type == 'error')
				sweet_alert(resp.type, resp.message);
			else{
				//open modal, append get values
				val = resp.message;
				$('#addNavigation form [name=title]').val(val.title);
				$('#addNavigation form [name=group]').val(val.group_id);
				$('#addNavigation form [name=url]').val(val.url);
				edit_action(group, form_target, val.parent);

			}
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	});


	function edit_action(group, target, parent_value=0){
		var dataid = group;
		var form_add_action = '';

		//load parent ajax value
		$.ajax({
			url : ADMIN_URL + '/navigation/list-navigation/'+dataid,
			type : 'POST',
			dataType : 'json'
		}).done(function(resp){
			options = '';
			$.each(resp, function(key, val){
				if(parent_value == key)
					options += '<option '+parent_value+' selected value="'+key+'">'+val+'</option>';
				else
					options += '<option '+parent_value+' value="'+key+'">'+val+'</option>';
			});
			$("#addNavigation form").attr('action', target);
			$("#addNavigation form [name=parent]").html(options);
			$("#addNavigation h4.title").html('Update Navigation');

			$("#addNavigation [name=group]").attr('disabled', 'disabled');

			//load modal
			$("#addNavigation").modal('show');
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	}


	$(".delete-button").on('click', function(e){
		e.preventDefault();
		var post_id = $(this).data('id');

		if(post_id > 0){
		alertify.confirm('Hapus Data?', 'Are you really want to delete this navigation item? Warning : All the navigation children will be deleted too.', function(){
			
			$.ajax({
				url : ADMIN_URL + 'navigation/detail/destroy/' + post_id,
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(resp){
				sweet_alert(resp.type, resp.message);
				if(resp.type == 'success')
					location.reload();
			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});

		}, function(){});
	}
	});
</script>
@endpush