@section ('modal')
	<div id="groupModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				{{ Form::open(['method' => 'POST', 'class' => 'ajax-request']) }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Add Group Navigation</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-3">
							Title
						</div>
						<div class="col-sm-9">
							<input type="text" name="title" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							Description
						</div>
						<div class="col-sm-9">
							<textarea name="description" class="form-control"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">
						<span class="fa fa-save"></span>
						Simpan
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@endsection