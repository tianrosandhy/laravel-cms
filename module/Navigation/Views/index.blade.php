@extends('admin.inc.template')
@section('content')

	<h2>Navigation Group</h2>

	@if(hasAccess('admin.navigation.store'))
	<a data-toggle="modal" data-target="#groupModal" class="btn btn-primary btn-add">
		<i class="fa fa-plus"></i>
		Add Navigation Group
	</a>
	@endif

	{!! $table !!}

	@include ('navigation::create')
@endsection


@push ('script')
<script>
	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'POST',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '/navigation/switch'
		}).done(function(dt){
			msghandling(dt);
		}).fail(function(dt){
			swal('Error ' + dt.status, dt.responseText, 'warning');
		});
	});

	$("body").on("click", ".edit-btn", function(){
		var ini = $(this);
		post_id = ini.data('id');

		$("#groupModal h3.modal-title").html('Update Data');

		if(post_id > 0){
			$.ajax({
				url : ADMIN_URL + '/navigation/edit',
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				$.each(dt, function(nm, ctn){
					$("[name="+nm+"]").val(ctn);
				});
				$("#groupModal form").attr('action', ADMIN_URL + '/navigation/update/'+post_id);
				$("#groupModal").modal();
			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});
		}
	});

	$(".btn-add").on('click', function(){
		$("#groupModal h3.modal-title").html('Tambah Data Group Navigation');
		data = $("#groupModal form").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-request").attr('action', '');
	});

	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "/navigation/destroy",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					swal('Error ' + dt.status, dt.responseText, 'warning');
				});

			}, function(){});
		}
	});
</script>
@endpush