<?php

namespace Module\Setting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use Module\Setting\Models\UserModel;
use Module\Setting\Traits\UserDatatable;
use DataTable;

class SettingUserController extends Controller
{
	use UserDatatable;

	public 	$request, 
			$forms,
			$model;
	
    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new UserModel();
        DataTable::set('columns', $this->columns);
        DataTable::set('model', $this->model);
        DataTable::set('data', $this->request->all());

		$this->registerForm();
	}

    public function index(){
    	return view('setting::user')->with([
    		'title' => 'User Setting',
    		'forms' => $this->forms,
    		'table' => DataTable::tableGenerate([
    			'server' => '/setting/user/table',
    			'hideOrder' => [4],
    			'custom' => [
    				['priviledge', 'users_priviledge', 'priviledge_name', 'id']
    			]
    		])
    	]);

    }

    public function store(){
    	$rule = [
    		'username' => 'required|max:60',
    		'email' => 'required|email',
    		'pass' => 'required|min:5|confirmed',
    		'priviledge' => 'required|integer',
    	];

    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('username', $this->request->username)
			->get();

		if(count($cek) > 0){
			return ajax_response('Data user dengan email / username tersebut sudah ada.');
		}
		else{
			$pass = password_hash($this->request->pass, PASSWORD_DEFAULT);
			$remember = sha1($pass);
			//proses simpan
			$this->model->insert([
				'username' => $this->request->username,
				'email' => $this->request->email,
				'password' => $pass,
				'remember_token' => $remember,
				'created_at' => date("Y-m-d H:i:s"),
				'priviledge' => intval($this->request->priviledge)
			]);

			return ajax_response("Berhasil menyimpan data user", 'success');
		}
    }



    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
    		'username' => 'required|max:60',
    		'email' => 'required|email',
    		'priviledge' => 'required|integer',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('username', $this->request->username)
			->where('id','<>',$id)
			->get();

		if(count($cek) > 0){
			return ajax_response('Data user dengan email / username tersebut sudah ada.');
		}
		else{
			$data = [
				'username' => $this->request->username,
				'email' => $this->request->email,
				'priviledge' => intval($this->request->priviledge)
			];
			if(strlen($this->request->pass) > 0 || strlen($this->request->pass_confirmation) > 0){

				$pass1 = $this->request->pass;
				$pass2 = $this->request->pass_confirmation;

				if($pass1 <> $pass2){
					return ajax_response('Password mismatch.');
				}
				elseif(strlen($pass1) < 5){
					return ajax_response("The password need at least 5 character");
				}
				else{
					$pass = password_hash($pass1, PASSWORD_DEFAULT);
					$data['password'] = $pass;
				}
			}

			$this->model->where('id',$id)->update($data);
			return ajax_response("Berhasil mengupdate data user", "success");
		}


    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

}
