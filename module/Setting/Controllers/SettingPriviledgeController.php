<?php

namespace Module\Setting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use Module\Setting\Models\UserModel;
use Module\Setting\Models\UserPriviledgeModel;
use Module\Setting\Traits\UserPriviledgeTrait;
use Module\Setting\Traits\UserPriviledgeDatatable;
use DataTable;

class SettingPriviledgeController extends Controller
{
	public 	$request, 
			$forms,
			$model;

	use UserPriviledgeTrait;
	use UserPriviledgeDatatable;
	
    //
	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new UserPriviledgeModel();
        DataTable::set('columns', $this->columns);
        DataTable::set('model', $this->model);
        DataTable::set('data', $this->request->all());

		$this->registerForm();
	}

    public function index(){
    	return view('setting::priviledge')->with([
    		'title' => 'Priviledge Group',
    		'forms' => $this->forms,
    		'priviledge_list' => $this->get_priviledge_list(),
    		'table' => DataTable::tableGenerate([
    			'server' => '/setting/priviledge/table',
    			'hideOrder' => [1]
    		]),
    	]);
    }

    public function store(){
    	$rule = [
    		'priviledge_name' => 'required|max:80',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('priviledge_name', $this->request->priviledge_name)
			->get();

		if(count($cek) > 0){
			return ajax_response("Group priviledge dengan nama tersebut sudah digunakan");
		}
		else{
			//proses simpan
			$this->model->insert([
				'priviledge_name' => $this->request->priviledge_name,
				'type' => 1,
				'stat' => 1
			]);
			return ajax_response("Berhasil menyimpan group priviledge baru", "success");
		}

    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
    		'priviledge_name' => 'required|max:80',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			return ajax_response($validate->messages()->first());
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('priviledge_name', $this->request->priviledge_name)
			->where('id','<>',$id)
			->get();

		if(count($cek) > 0){
			return ajax_response("Data group permission dengan nama tersebut sudah ada.");
		}
		else{
			$data = $this->model->find($id);
			if($data->type == 0){
				return ajax_response("Mohon maaf, grup user Admin utama tidak dapat diupdate");
			}
			else{
				$data->priviledge_name = $this->request->priviledge_name;
				$data->save();
				return ajax_response("Berhasil mengupdate priviledge name", "success");
			}

		}

    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek->type > 0){
		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Tidak dapat menghapus group priviledge admin utama";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    public function manage($id){
    	$post = $this->request->all();
    	unset($post['_token']);

    	$saved = [];
    	foreach($post as $routename => $val){
    		$saved[] = str_replace("_", ".", $routename);
    	}

    	$saved = json_encode($saved);
    	$current_user = $id;

    	$user = $this->model->find($current_user);
    	$user->permission = $saved;
    	$user->save();

    	return ajax_response('Berhasil mengatur group permission priviledge', 'success');
    }
}
