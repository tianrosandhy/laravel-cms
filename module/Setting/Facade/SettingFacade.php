<?php 

namespace Module\Setting\Facade;

use Illuminate\Support\Facades\Facade;

class SettingFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Setting\\Controllers\\SettingController';
    }
}