<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Priviledge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_priviledge', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('priviledge_name',190);
            $tb->text('permission')->nullable();
            $tb->timestamps();
            $tb->tinyinteger('type');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_priviledge');
    }
}
