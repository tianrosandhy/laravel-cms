<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->get('setting', 'SettingController@index')->name('admin.setting.setting.index');
	$route->post('setting', 'SettingController@store')->name('admin.setting.setting.store');



	$route->get('setting/user', 'SettingUserController@index')->name('admin.setting.user.index');
	$route->post('setting/user', 'SettingUserController@store')->name('admin.setting.user.store');
	$route->post('setting/user/table', 'SettingUserController@getPost');
	$route->post('setting/user/switch', 'SettingUserController@getSwitch');
	$route->post('setting/user/edit', 'SettingUserController@edit')->name('admin.setting.user.edit');
	$route->post('setting/user/update/{id?}', 'SettingUserController@update')->name('admin.setting.user.update');
	$route->post('setting/user/destroy', 'SettingUserController@destroy')->name('admin.setting.user.destroy');



	$route->get('setting/priviledge', 'SettingPriviledgeController@index')->name('admin.setting.priviledge.index');
	$route->post('setting/priviledge', 'SettingPriviledgeController@store')->name('admin.setting.priviledge.store');
	$route->post('setting/priviledge/table', 'SettingPriviledgeController@getPost');
	$route->post('setting/priviledge/switch', 'SettingPriviledgeController@getSwitch');
	$route->post('setting/priviledge/edit', 'SettingPriviledgeController@edit')->name('admin.setting.priviledge.edit');
	$route->post('setting/priviledge/update/{id?}', 'SettingPriviledgeController@update')->name('admin.setting.priviledge.update');
	$route->post('setting/priviledge/manage/{id?}', 'SettingPriviledgeController@manage')->name('admin.setting.priviledge.manage');
	$route->post('setting/priviledge/getpriv/{id?}', 'SettingPriviledgeController@get_user_priviledge');

	$route->post('setting/priviledge/destroy', 'SettingPriviledgeController@destroy')->name('admin.setting.priviledge.destroy');




});
