<div class="modal fade" id="manageForm">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{{ Form::open(['method' => 'POST', 'class' => 'ajax-request']) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Manage Group Permission</h3>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>Module</th>
							<th>Permission</th>
						</tr>
					</thead>
					<tbody>
						
					@foreach($priviledge_list as $label => $data)
						@if(count($data) > 0)
						<tr>
							<td>
								<label class="main-label"><input class="group-click" type="checkbox" id="{{ $label."_" }}"> {{ ucfirst($label) }}</label>
							</div>
							<td>
							@foreach($data as $key=>$val)
								@if(count($val) > 0)
									<label><em><b><input class="group-click" type="checkbox" id="{{ $label."_".$key."_" }}"> {{ ucfirst($key) }}</b></em></label>
									<br>
									@foreach($val as $k => $v)
										<label class="ctrl-label" for="{{ $label."_".$key."_".$k }}">
											<input type="checkbox" id="{{ $label."_".$key."_".$k }}" name="{{ $label."_".$key."_".$k }}">
											{{ $k }}
										</label>
									@endforeach
									<br>
								@else
									<label class="ctrl-label" for="{{ $label."_".$key }}">
										<input type="checkbox" id="{{ $label."_".$key }}" name="{{ $label."_".$key }}">
										{{ $key }}		
									</label>
								@endif
							@endforeach							
							</td>
						</tr>
						@endif
					@endforeach

					</tbody>
				</table>


			</div>
			<div class="modal-footer">
				@if(hasAccess('admin.setting.priviledge.manage'))
				<button type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span>
					Simpan
				</button>
				@endif
				<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</form>
		</div>
	</div>
</div>