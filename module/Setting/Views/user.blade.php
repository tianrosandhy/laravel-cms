@extends('admin.inc.template')
@section('content')

	@if(hasAccess('admin.setting.user.store'))
	<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
		Tambah Data
	</a>
	<br>
	@endif

	<hr>

	{!! $table !!}

	@include ('setting::create-user')

@stop

@push('script')
<script>
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-request").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-request").attr('action', '');
		$(".row.append").hide();

		form_refresh();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "/setting/user/edit",
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$(".modal [name="+nm+"]").val(ctn);
					}
				});
				form_refresh();

			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});

			$(".ajax-request").attr('action', ADMIN_URL + '/setting/user/update/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "/setting/user/destroy",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					swal('Error ' + dt.status, dt.responseText, 'warning');
				});

			}, function(){});
		}
	});

</script>
@endpush