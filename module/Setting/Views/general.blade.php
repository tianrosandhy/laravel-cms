@extends('admin.inc.template')
@section('content')
	<h2>{{ $title }}</h2>

	@foreach($form as $group_name => $item)
	<div class="card">
		<div class="card-header separator">
			<div class="card-title">{{ $group_name }}</div>
		</div>
		<div class="card-block">
			<br>
			<form action="{{ route('admin.setting.setting.store') }}" method="post">
				{{ csrf_field() }}
				@foreach($item as $name => $data)
				<div class="form-group">
					<div class="row" title="{{ $data['description'] }}">
						<div class="col-sm-3">
							<label>{{ $data['label'] }}</label>
						</div>
						<div class="col-sm-9">
						@if($data['type'] == 'select')
							{!! Form::select($name, $data['list'], (isset($filled[$name]) ? $filled[$name] : $data['default']), $data['attr']) !!}
						@elseif($data['type'] == 'textarea')
							{!! Form::textarea($name, (isset($filled[$name]) ? $filled[$name] : $data['default']), $data['attr']) !!}
						@elseif($data['type'] == 'cropper')
							@include ('admin.inc.partials.cropper-box', [
								'ratio' => isset($data['ratio']) ? $data['ratio'] : [800, 600],
								'value' => (isset($filled[$name]) ? $filled[$name] : ''),
								'name' => $name,
							])
						@elseif($data['type'] == 'file')
				       <div class="row">
				        <div class="col-sm-6">
				         <input type="hidden" name="{{ $name }}" value="{{ isset($filled[$name]) ? $filled[$name] : '' }}">
				         <div class="dropzone dz-clickable setting-{{$name}}" data-target="/api/gallery">
                    <div class="dz-default dz-message">
                        <span>Drop files here to upload</span>
                    </div>
                </div>
                <!-- Langsung generate script unique target -->
                <script>
                $(function(){
				             Dropzone.autoDiscover = false;
				             var ajaxurl = ADMIN_URL + $(".setting-{{$name}}").data("target");
				             $(".setting-{{$name}}").dropzone({
				                 url : ajaxurl,
				                 sending : function(file, xhr, formData){
				                     formData.append("_token", $('meta[name="csrf_token"]').attr('content'));
				                 },
				                 init : function(){
				                     this.on("success", function(file, data){
				                         $("[name='{{$name}}']").val(data);
				                     });
				                     this.on("addedfile", function() {
				                       if (this.files[1]!=null){
				                         this.removeFile(this.files[0]);
				                       }
				                     });
				                 }
				             });
                  });
                  </script>				 
				        </div>
				        <div class="col-sm-6">
				         @if(isset($filled[$name]))
				         <b>Uploaded Image</b>
				         <br>
				         <img src="{{ url('storage/thumb/thumb-100-'.$filled[$name]) }}">
				         @endif
				        </div>
				       </div>
						@else
							{{ Form::input($data['type'], $name, (isset($filled[$name]) ? $filled[$name] : $data['default']), $data['attr']) }}
						@endif
						</div>
					</div>
				</div>
				@endforeach
				
				@if(hasAccess('admin.setting.setting.store'))
				<div class="row">
					<div class="col-sm-9 offset-sm-3">
						<button class="btn btn-primary">
							<i class="fa fa-save"></i>
							Simpan
						</button>
					</div>
				</div>
				@endif
			</form>
		</div>
	</div>
	@endforeach

@include ('admin.inc.cropper')
@endsection
