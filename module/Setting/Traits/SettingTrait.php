<?php
namespace Module\Setting\Traits;

use Auth;
use Form;
use Request;
use Module\Setting\Models\SettingModel;
use Module\Setting\Models\UserPriviledgeModel;

trait SettingTrait
{
	static $excepted_route = [
        '',
        'dashboard',
        'login',
        'postlogin',
        'logout',
    ];


	//static facade method only
	static function getParam($param_name, $ifblank=""){
		$sql = SettingModel::where('param', $param_name)->where('stat', 1)->first();
		if(count($sql) == 1 && strlen($sql) > 0){
			return $sql->value;
		}
		else{
			//get from config
			$value = self::get_config_item($param_name);
			if(strlen($value) > 0)
				return $value;
		}
		return $ifblank;
	}

	static function setParam($param_name, $value='', $force=false){
		$cek = SettingModel::where('param', $param_name)->where('stat', 1)->get();
		if(count($cek) == 1){
			SettingModel::where('param', $param_name)
				->update(['value' => $value]);
			return true;
		}
		else{
			if($force == true){
				SettingModel::insert([
					'param' => $param_name,
					'value' => $value,
					'created_at' => date('Y-m-d H:i:s'),
					'stat' => 1
				]);
				return true;
			}
			return false;
		}
	}

	static function get_config_item($name, $get='default'){
		$config = config('setting');
		foreach($config as $group => $item){
			if(isset($item[$name])){
				//ketemu
				return $item[$name][$get];
			}
		}
		return null;
	}

	static function adminCheckPermission($routename, $additional=[]){
		$data = UserPriviledgeModel::where([
            'id' => Auth::user()->priviledge
        ])->first();

        $permission_list = json_decode($data->permission);		

        if(empty($routename)){//empty route berarti main menu yang memiliki children
        	if(count($additional) > 0){
                $ret = false;
                foreach($additional as $add){
                    $rtname = str_replace('admin.', '', $add);
                    if(in_array($rtname, self::$excepted_route)){
                        $ret = true;
                    }
                    else if(in_array($rtname, $permission_list))
                        $ret = true;
                }
                return $ret;
            }
            return true;
        }

        $routename = str_replace('admin.', '', $routename);
        if(in_array($routename, self::$excepted_route)){
            return true;
        }

        #just for admin
        if(Auth::user()){
            
            if(in_array($routename, $permission_list)){
                return true;
            }
        }
        return false;
    }

}