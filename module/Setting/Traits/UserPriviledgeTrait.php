<?php
namespace Module\Setting\Traits;

use Route;
use Module\Setting\Models\UserPriviledgeModel;

trait UserPriviledgeTrait
{

	public function get_user_priviledge($id){
		$data = $this->model->find($id);
		if(strlen($data->permission) == 0)
			return [];

		$permission = json_decode($data->permission, true);

		//sebelum lupa, jangan lupa tanda titik dibalikin ke underscore lagi
		$imp = [];
		foreach ($permission as $value) {
			$imp[] = str_replace(".", "_", $value);
		}		

		return json_encode($imp);
	}	

	public function get_priviledge_list($except=['dashboard','login','postlogin','api']){
		//FUCKING METHOD TO STRUCTURIZED PRIVILEDGE IN ROUTE LISTS

		$routeCollection = Route::getRoutes();

		$save = [];
		foreach ($routeCollection as $value) {
			$routename = $value->getName();
			if(strlen($routename) > 0){
				$x[] = explode(".", $routename);
			}

		}

		$a = array_unique(array_column($x, 1));
		$a = array_flip($a);
		foreach($a as $k=>$aa){
			$a[$k] = [];
		}


		foreach($x as $row){
			if(isset($row[1])){
				if(array_key_exists($row[1], $a)){
					if(isset($row[2])){
						$a[$row[1]][$row[2]] = [];
						if(isset($row[3])){
							$baru[$row[1]][$row[2]][$row[3]] = [];
						}
					}
				}
			}

		}


		$last = array_merge($a, $baru);
		foreach($last as $key=>$module){
			if(in_array($key, $except)){
				unset($last[$key]);
			}
		}

		//jadi, seluruh route list ada di $last
		return $last;
	}

}