<?php
namespace Module\Setting\Traits;

use DataTable;
use Module\Setting\Models\UserPriviledgeModel;
use Form;

trait UserPriviledgeDatatable
{
	public $columns = [
		'Priviledge Name' => ['search' => true, 'col' => 'priviledge_name'],
		'' => ['search' => false, 'col' => 'button'], 
	];

	public function registerForm(){
		$this->forms = [
			'Priviledge Name' => Form::input('text', 'priviledge_name', old('priviledge_name', null), ['class' => 'form-control'])
		];
	}

	
	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate();
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$btn = '';
				if(hasAccess('admin.setting.priviledge.update')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.setting.priviledge.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}

				$filtered['data'][] = array(
					$row->priviledge_name,
					'<div class="btn-group">
						<a data-id="'.$row->id.'" class="btn-info btn btn-sm waves-effect manage-btn" data-toggle="modal" data-target="#manageForm">Manage Permission</a>
						'.$btn.'						
					</div>'
				);
			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}