<?php
namespace Module\Setting\Traits;

use DataTable;
use Module\Setting\Models\UserModel;
use Form;

trait UserDatatable
{
	public $columns = [
		'Username' => ['search' => true, 'col' => 'username'],
		'Email' => ['search' => true, 'col' => 'email'],
		'Created At' => ['search' => true, 'col' => 'created_at'],
		'Priviledge' => ['search' => true, 'col' => 'priviledge', 'custom' => true],
		'' => ['search' => false, 'col' => 'button'], 
	];

	public function registerForm(){
		$this->forms = [
			'Username' => Form::input('text', 'username', old('username', null), ['class' => 'form-control']),

			'Email' => Form::input('email', 'email', old('email', null), ['class' => 'form-control']),

			'Password' => Form::input('password', 'pass', old('pass1', null), ['class' => 'form-control']),

			'Repeat Password' => Form::input('password', 'pass_confirmation', old('pass2', null), ['class' => 'form-control']),

			'Priviledge' => Form::select('priviledge', get_list('users_priviledge', 'priviledge_name'), old('priviledge', null), ['class' => 'form-control']),

		];		
	}

	
	// Public Method utk generate query datatable
	public function getPost(){
		$filtered = DataTable::generate(false);
		return self::tableFormat($filtered);
	}


	// Protected method utk mengolah hasil query 
	protected function tableFormat($filtered){
		if(count($filtered['result']) > 0){
			$filtered['data'] = [];
			foreach($filtered['result'] as $row){
				$privname = DataTable::get_from_tb('users_priviledge',$row->priviledge, 'priviledge_name');

				if(strlen($privname) == 0){
					$privname = '<div class="label label-danger">New Regitered User</div>';
				}

				$filtered['data'][] = array(
					$row->username,
					$row->email,
					indo_date($row->created_at),
					$privname,

					'<div class="btn-group">
						<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>
						<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>
					</div>'
				);
			}
		}
		//always unset $filtered['result'] before exit
		unset($filtered['result']);

		return $filtered;
	}

}