<?php

namespace Module\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class UserModel extends Model
{
    //
    protected $table = "users";
    protected $fillable = [
    	'username',
    	'email',
    	'password',
    	'remember_token',
    	'created_at',
    	'updated_at',
    	'priviledge'
    ];

    public function priviledge(){
        return $this->belongsto('Module\Setting\Models\UserPriviledgeModel');
    }


}
