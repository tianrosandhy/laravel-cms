<?php

namespace Module\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class UserPriviledgeModel extends Model
{
    //
    protected $table = "users_priviledge";
    protected $fillable = [
        'id',
    	'priviledge_name',
    	'permission',
    	'created_at',
    	'updated_at',
        'type',
    	'stat'
    ];
    
    

}
