<?php

namespace Module\Setting\Models;

use Illuminate\Database\Eloquent\Model;


class SettingModel extends Model
{
    //
    protected $table = "setting";
    protected $fillable = [
    	'id',
    	'param',
    	'value',
    	'created_at',
    	'updated_at',
    	'stat'
    ];


}
